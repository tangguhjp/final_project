<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlternativeValue extends Model
{
    protected $table = 'alternative_value';
    public $timestamps = false;
}
