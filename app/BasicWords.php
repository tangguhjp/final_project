<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasicWords extends Model
{
    protected $table = 'word_dictionary';
    public $timestamps = false;
}
