<?php

namespace App\Classes;

use Abraham\TwitterOAuth\TwitterOAuth; 
use GuzzleHttp\Client;

class SocialMedia{
    public static function getTwitterPost($keyword){
        $data = new \StdClass();
        $data->status = 200;

        $CONSUMER_KEY = 'NlubPgLiPcgoGAOBrXWnlG7y3';
        $CONSUMER_SECRET_KEY = 'DQhnjf7toJv0pF5RwkJ4AEHuRE3OIDbAf2kyUFV7dQvtOdaHPV';
        $ACCESS_TOKEN = '802472623-oYwwFOusVPWfXGr36p3dZCSMgU4nCJlptNOxIj2y';
        $ACCESS_TOKEN_SECRET = 'j14yrWmo3MgmiPYeTThJYluLERTqBuh2ZpcItOKLwyGmR';
 
        $connection = new TwitterOAuth(
            $CONSUMER_KEY, $CONSUMER_SECRET_KEY, 
            $ACCESS_TOKEN, $ACCESS_TOKEN_SECRET);
        $query = array(
             "q" => $keyword,
             "count" => 15,
             "result_type" => "mixed",
             "lang" => "id",
             "tweet_mode" => "extended"
        );
        $tweets = $connection->get("search/tweets", $query);
        // return response()->json($tweets);
        if(count($tweets->statuses) > 0){
            foreach($tweets->statuses as $s){
                $tweet = new \StdClass();
                $tweet->text = $s->full_text;
                $tweet->time = strtotime($s->created_at);
                $tweet->user = $s->user->name;
                $dataTweets[] = $tweet;
            }
            $data->data = $dataTweets;
        }else{
            $data->status = 300;
            $data->data = [];
        }
        return $data;
    }

    public function getFoursquareVenue($keyword){
        $client = new Client();
        $form_params = [
            'form_params' => [
                "oauth_token" => 'XEAYMZ4GT45YKWPMQBJ4JRJUWAIWHRQ2IVAHCTG4B5UPUEP2',
                "v" => 20160620,
                "ll" => "-7.3042731,112.7857747",
                "query" => $keyword
            ]
        ];

        $res = $client->request('POST', 'https://api.foursquare.com/v2/venues/search', $form_params);

        $data = json_decode($res->getBody(), true)['response']['venues'];

        foreach($data as $d){
            if($this->validateVenue($keyword, $d['name'])){
                $listId[] = $d['id'];
            }
        }

        foreach($listId as $id){
            $tips[] = $this->getVenueTips($id);
        }
        return $tips;
    }

    public function getVenueTips($id){
        $client = new Client();
        $params = [
            'query' => [
                "oauth_token" => 'XEAYMZ4GT45YKWPMQBJ4JRJUWAIWHRQ2IVAHCTG4B5UPUEP2',
                "v" => 20160620,
                "limit" => 100,
                "offset" => 100
            ]
        ];

        $res = $client->request('GET', 'https://api.foursquare.com/v2/venues/'.$id.'/tips', $params);

        $data = json_decode($res->getBody(), true)['response']['tips']['items'];
        foreach($data as $d){
            $tips[] = $d['text'];
        }
        return $tips;
    }
    
    public static function validateVenue($keyword, $name){
        $keyword = trim($keyword); //remove whitespace in the beginning of string
        $keyword = rtrim($keyword); //remove whitespace in the end of string
        $keyword = str_replace(' ', '|', $keyword);
        preg_match_all('/('.$keyword.')/i', $name, $match);
        if(str_word_count($keyword) == count(array_shift($match))){
            return true;
        }
        return false;
    }
}