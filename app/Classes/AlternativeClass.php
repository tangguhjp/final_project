<?php
namespace App\Classes;
use App\User;
use App\Alternative;
use Auth;

class AlternativeClass{
    public static function getAllAlternativeId(){
        return Alternative::where('id_user', Auth::id())->pluck('id')->all();
    }

    public static function getAlternativeName($id){
        return Alternative::find($id)->name;
    }
}