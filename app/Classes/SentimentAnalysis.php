<?php
namespace App\Classes;

use App\Classes\Words;
use App\Classes\TextMining;

use App\StandardWords;
use App\SentimentalWords;
use App\Stoplists;
use App\BasicWords;

class SentimentAnalysis {
    private $WORDS;
    private $TEXT_MINING;
    public function __construct()
    {
        $this->WORDS = new Words();
        $this->TEXT_MINING = new TextMining();
    }

    public function process($text){
        $text = preg_replace('/[[:^print:]]/', '', $text);
        $text = $this->TEXT_MINING->correctWord($text);
        // $text = $this->TEXT_MINING->spellCheck($text);

        $text = str_replace("\r\n",'', $text);
        $text = str_replace(".",' . ', $text);
        $text = str_replace(",",' , ', $text);
        $text = str_replace("/",'', $text);
        $text = str_replace("!",' ! ', $text);
        $text = str_replace("?",' ? ', $text);
        $text = str_replace("2",'', $text);

        //mendapatkan kata baku
        $text = $this->standardWords($text);
    
        $hasil = [];
        for($i = 0; $i < count($text); $i++){
            if($this->WORDS->checkSentimentalWords($text[$i])){
                $hasil[$i] = strtolower($text[$i]);
            }else if($this->WORDS->checkStoplist($text[$i])){
                $hasil[$i] = strtolower($this->nazief($text[$i]));

                if(strtolower($text[$i]) != $hasil[$i] && $hasil[$i] != '')
                    $imbuhan[$i] = strtolower($text[$i]).' = '.$hasil[$i];
                //preg_match check number
                if(is_null($hasil[$i]) || $hasil[$i] == '' || preg_match('/^[0-9]{1,}$/', $hasil[$i]))
                {
                    unset($hasil[$i]);
                }
            }else{
                $stoplists[$i] = strtolower($text[$i]);
            }
        }
        $hasil = $this->reconstructArray($hasil);
        $result = $this->mainSentimentAnalysis($hasil);
        return $result;
    }

    //mendapatkan kata baku
    public function standardWords($text){
        $baku = [];
        $text = explode(" ", $text);
        for($i = 0; $i < count($text); $i++){
            $baku[$i] = $this->checkStandardWords($text[$i]);
        }
        return $baku;
    }

    public function nazief($word){
        if($this->WORDS->checkSentimentalWords($word) || $this->WORDS->checkBasicWords($word)){
            return $word;
        }else{
            $word = $this->TEXT_MINING->deleteInflectionSuffixes($word);
            $word = $this->TEXT_MINING->deleteDerivationSuffixes($word);
            $word = $this->TEXT_MINING->deleteDerivationPrefixes($word);
            return $word;
        }
    }

    public function reconstructArray($array){
        $i = 0;
        $result = [];
        foreach($array as $row)
        {
            $result[$i] = $row;
            $i++;
        }
        return $result;
    }

    public function mainSentimentAnalysis($sentence){
        $i = 0;
        $wordList = [];
        $result = [];
        $score = [];
        foreach($sentence as $word){
            $wordList[$i] = $this->WORDS->getSentimentalWords($word);
            $i++;
        }        

        for($i = 0; $i < count($wordList); $i++)
        {
            //cek verba
            if($this->WORDS->checkVerb($wordList[$i]))
            {
                //cek ada keterangan sebelum verba
                if($i != 0 && $i != count($wordList)-1) // jika tidak di awal kalimat
                {
                    if($this->WORDS->checkAdverb($wordList[$i-1]))
                    {
                        error_reporting(0);
                        if($this->WORDS->checkAdjective($wordList[$i+1]))
                        {
                            $verb_adj = $this->countLogic($this->getValue($wordList[$i]), $this->getValue($wordList[$i+1]), 'after');
                            $score[$i] = $this->countLogic($this->getValue($wordList[$i-1]), $verb_adj, 'before');
                            $i++;
                        }
                        else
                        {
                            $score[$i] = $this->countLogic($this->getValue($wordList[$i-1]), $this->getValue($wordList[$i]), 'before');
                        }
                    }
                    elseif($this->WORDS->checkAdjective($wordList[$i+1]))
                    {
                        $score[$i] = $this->countLogic($this->getValue($wordList[$i]), $this->getValue($wordList[$i+1]), 'after');
                        $i++;
                    }
                    else
                    {
                        $score[$i] = $this->getValue($wordList[$i]);
                    }
                }

                //cek ada adjektiva sesudah verba
                elseif($i != count($wordList)-1) //jika tidak diakhir kalimat
                {
                    if($this->WORDS->checkAdjective($wordList[$i+1]))
                    {
                        echo 'masuk';
                        $score[$i] = $this->countLogic($this->getValue($wordList[$i]), $this->getValue($wordList[$i+1]), 'after');
                        $i++;
                    }
                    else
                    {
                        $score[$i] = $this->getValue($wordList[$i]);
                    }
                }
                else
                {
                    $score[$i] = $this->getValue($wordList[$i]);
                }
            }

            //cek adjektiva
            elseif($this->WORDS->checkAdjective($wordList[$i]))
            {
                //cek ada keterangan sebelum adjektiva
                if($i != 0) // jika tidak di awal kalimat
                {
                    if($this->WORDS->checkAdverb($wordList[$i-1]))
                    {
                        error_reporting(0);
                        if($this->WORDS->checkVerb($wordList[$i+1]))
                        {
                            $pre_adj = $this->countLogic($this->getValue($wordList[$i-1]) , $this->getValue($wordList[$i]), 'after');
                            $score[$i] = $this->countLogic(intval($pre_adj), $this->getValue($wordList[$i+1]), 'before');
                            $i++;
                        }
                        else
                        {
                            $score[$i] = $this->countLogic($this->getValue($wordList[$i-1]), $this->getValue($wordList[$i]), 'before');
                        }
                    }
                    else
                    {
                        $score[$i] = $this->getValue($wordList[$i]);
                    }   
                }
                //cek ada verba sesudah adjektiva
                elseif($i != count($wordList)-1) //jika tidak di akhir kalimat
                {
                    if($this->WORDS->checkVerb($wordList[$i+1]))
                    {
                        $score[$i] = $this->countLogic($this->getValue($wordList[$i]), $this->getValue($wordList[$i+1]), 'after');
                        $i++;
                    }
                    else
                    {
                        $score[$i] = $this->getValue($wordList[$i]);
                    }
                }
                else
                {
                    $score[$i] = $this->getValue($wordList[$i]);
                }
            }

            elseif($this->getValue($wordList[$i]) != 0 && !$this->WORDS->checkAdverb($wordList[$i]))
            {
                $score[$i] = $this->getValue($wordList[$i]);
            }
        }
        $result['wordlist'] = $wordList;
        $result['scorelist'] = $this->reconstructArray($score);
        $result['score'] = array_sum($score);
        return $result;
    }

    public static function getValue($word){
        return intval($word['value']);
    }

    function countLogic($x, $y, $type){
        //positif ketemu positif
        if($x == 1 && $y == 1)
        {
            if($type == 'before')
                $result = 1;
            elseif($type == 'after')
                $result = 1;
        }

        //positif ketemu negatif
        elseif($x == 1 && $y == -1)
        {
            if($type == 'before')
                $result = -1;
            elseif($type == 'after')
                $result = -1;
        }

        //negatif ketemu positif
        elseif($x == -1 && $y == 1)
        {
            if($type == 'before')
                $result = -1;
            elseif($type == 'after')
                $result = -1;
        }

        //negatif ketemu negatif
        elseif($x == -1 && $y == -1)
        {
            if($type == 'before')
                $result = 1;
            elseif($type == 'after')
                $result = -1;
        }

        elseif($x == 0)
            $result = $y;
        elseif($y == 0)
            $result = $x;
        return $result;
    }

    //cek kata baku dan mengembalikan kata baku
    public static function checkStandardWords($word){
        $data = StandardWords::where('kata', $word)->get()->first();
        if($data)
            return $data->kata_asli;
        return $word;
    }
}
