<?php
namespace App\Classes;
use App\SentimentalWords;
use App\Stoplists;
use App\BasicWords;

class Words{ 
    public static function checkSentimentalWords($word){
        $data = SentimentalWords::where('word', $word)->get()->first();
        if($data)
            return true;
        return false;
    }

    public static function checkStoplist($word){
        $data = Stoplists::where('stoplist', $word)->get()->first();
        if($data)
            return false;
        return true;
    }

    public static function checkBasicWords($word){
        $data = BasicWords::where('katadasar', $word)->get()->first();
        if($data)
            return true;
        return false;
    }

    public function getSentimentalWords($word){
        $result = [];
        $data = SentimentalWords::select('word', 'type', 'value')
            ->where('word', $word)->get()->first();
        if($data)
            return $data;
        $result['word'] = $word;
        $result['type'] = $this->getWordType($word);
        $result['value'] = 0;
        return $result; 
    }

    public static function getWordType($word){
        $data = BasicWords::where('katadasar', $word)->get()->first();
        if($data)
            return $data->tipe_katadasar;
        return "Unknown";
    }

    public static function checkVerb($word){
        if(strtolower($word['type'])=='verba')
            return true;
        return false;
    }

    public static function checkAdverb($word){
        if(strtolower($word['type'])=='keterangan')
            return true;
        return false;
    }

    public static function checkAdjective($word){
        if(strtolower($word['type'])=='adjektiva')
            return true;
        return false;
    }
}