<?php
namespace App\Classes;
use App\Classes\Words;

class TextMining{
    private $WORDS;

    public function __construct(){
        $this->WORDS = new Words();
    }

    function deleteInflectionSuffixes($word) 
    {
        if (preg_match('/([km]u|nya|[kl]ah|pun)$/i', $word)) 
        {
            $__word = preg_replace('/(nya|[kl]ah|pun)$/i', '', $word);
            if (preg_match('/([klt]ah|pun)$/i', $word))
            {
                if (preg_match('/([km]u|nya)$/i', $word))
                {
                    $__word__ = preg_replace('/([km]u|nya)$/i', '', $word);
                    return $__word__;
                }
            }
            return $__word;
        }
        return $word;      
    }

    function deleteDerivationSuffixes($kata) 
    {
        $kataAsal = $kata;
        if (preg_match('/(i|an)$/i', $kata)) 
        {
            
            $__kata = preg_replace('/(i|an)$/i', '', $kata);
            if ($this->WORDS->checkBasicWords($__kata))
            {
                return $__kata;
            }
            
            
            if (preg_match('/(kan)$/i', $kata)) 
            {
                $__kata__ = preg_replace('/(kan)$/i', '', $kata);
                if ($this->WORDS->checkBasicWords($__kata__)) 
                {
                    return $__kata__;
                }
            }
            if ($this->checkPrefixDisallowedSuffixes($kata)) 
            {
                return $kataAsal;
            }
        }
        return $kataAsal;
    }

    function deleteDerivationPrefixes($kata) 
    {
        $kataAsal = $kata;
        // Jika di-,ke-,se-
        if (preg_match('/^(di|[ks]e)/i', $kata)) 
        {
            $__kata = preg_replace('/^(di|[ks]e)/i', '', $kata);
            
            if ($this->WORDS->checkBasicWords($__kata)) 
            {
                return $__kata;
            }
            
            $__kata__ = $this->deleteDerivationSuffixes($__kata);
            if ($this->WORDS->checkBasicWords($__kata__)) 
            {
                return $__kata__;
            }
            
            if (preg_match('/^(diper)/i', $kata)) 
            {
                $__kata = preg_replace('/^(diper)/i', '', $kata);
                if ($this->WORDS->checkBasicWords($__kata)) 
                {
                    return $__kata;
                }
                
                $__kata__ = $this->deleteDerivationSuffixes($__kata);
                if ($this->WORDS->checkBasicWords($__kata__)) 
                {
                    return $__kata__;
                }
                
                $__kata = preg_replace('/^(diper)/i', 'r', $kata);
                if ($this->WORDS->checkBasicWords($__kata)) 
                {
                    return $__kata; // Jika ada balik
                }
                
                $__kata__ = $this->deleteDerivationSuffixes($__kata);
                if ($this->WORDS->checkBasicWords($__kata__)) 
                {
                    return $__kata__;
                }
            }
        }
        
        if (preg_match('/^([tmbp]e)/i', $kata)) 
        { 
            
            if (preg_match('/^(te)/i', $kata)) 
            { 
                if (preg_match('/^(terr)/i', $kata)) 
                {
                    return $kata;
                }
                
                if (preg_match('/^(ter)[aiueo]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(ter)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(ter[^aiueor]er[aiueo])/i', $kata)) 
                {
                    $__kata = preg_replace('/^(ter)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(ter[^aiueor]er[^aiueo])/i', $kata)) 
                {
                    $__kata = preg_replace('/^(ter)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata))
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(ter[^aiueor][^(er)])/i', $kata)) 
                {
                    $__kata = preg_replace('/^(ter)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(te[^aiueor]er[aiueo])/i', $kata)) 
                {
                    return $kata;
                }
                
                if (preg_match('/^(te[^aiueor]er[^aiueo])/i', $kata)) 
                {
                    $__kata = preg_replace('/^(te)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
            }
            
            if (preg_match('/^(me)/i', $kata)) 
            {
                if (preg_match('/^(meng)[aiueokghq]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(meng)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                    
                    $__kata = preg_replace('/^(meng)/i', 'k', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }

                if (preg_match('/^(meny)/i', $kata)) 
                {
                    $__kata = preg_replace('/^(meny)/i', 's', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(mem)[bfpv]/i', $kata)) 
                { // 3.
                    $__kata = preg_replace('/^(mem)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                    
                    $__kata = preg_replace('/^(mem)/i', 'p', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }

                    $__kata = preg_replace('/^(mempek)/i', 'k', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(men)[cdjsz]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(men)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(me)/i', $kata)) 
                {
                    $__kata = preg_replace('/^(me)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                    
                    $__kata = preg_replace('/^(men)/i', 't', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }

                    $__kata = preg_replace('/^(mem)/i', 'p', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
            }

            if (preg_match('/^(be)/i', $kata)) 
            {
                if (preg_match('/^(ber)[aiueo]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(ber)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata = preg_replace('/^(ber)/i', 'r', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }

                if (preg_match('/(ber)[^aiueo]/i', $kata)) 
                { // 2.
                    $__kata = preg_replace('/(ber)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata;
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) {
                        return $__kata__;
                    }
                }
                if (preg_match('/^(be)[k]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(be)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
            }
            
            if (preg_match('/^(pe)/i', $kata)) 
            {
                if (preg_match('/^(peng)[aiueokghq]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(peng)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }

                if (preg_match('/^(peny)/i', $kata)) 
                {
                    $__kata = preg_replace('/^(peny)/i', 's', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(pem)[bfpv]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(pem)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }

                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(pen)[cdjsz]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(pen)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                    
                    $__kata = preg_replace('/^(pem)/i', 'p', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                }

                if (preg_match('/^(pen)[aiueo]/i', $kata)) 
                {
                    $__kata = preg_replace('/^(pen)/i', 't', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(per)/i', $kata)) 
                {
                    $__kata = preg_replace('/^(per)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                    
                    $__kata = preg_replace('/^(per)/i', 'r', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
                
                if (preg_match('/^(pe)/i', $kata)) 
                {
                    $__kata = preg_replace('/^(pe)/i', '', $kata);
                    if ($this->WORDS->checkBasicWords($__kata)) 
                    {
                        return $__kata; // Jika ada balik
                    }
                    
                    $__kata__ = $this->deleteDerivationSuffixes($__kata);
                    if ($this->WORDS->checkBasicWords($__kata__)) 
                    {
                        return $__kata__;
                    }
                }
            }

            if (preg_match('/^(memper)/i', $kata)) 
            {
                $__kata = preg_replace('/^(memper)/i', '', $kata);
                if ($this->WORDS->checkBasicWords($__kata)) 
                {
                    return $__kata; // Jika ada balik
                }
                
                $__kata__ = $this->deleteDerivationSuffixes($__kata);
                if ($this->WORDS->checkBasicWords($__kata__)) 
                {
                    return $__kata__;
                }
                
                $__kata = preg_replace('/^(memper)/i', 'r', $kata);
                if ($this->WORDS->checkBasicWords($__kata)) 
                {
                    return $__kata; // Jika ada balik
                }
                
                $__kata__ = $this->deleteDerivationSuffixes($__kata);
                if ($this->WORDS->checkBasicWords($__kata__)) 
                {
                    return $__kata__;
                }
            }
        }
        
        /* --- Cek Ada Tidaknya Prefik/Awalan ------ */
        if (preg_match('/^(di|[kstbmp]e)/i', $kata) == FALSE) 
        {
            return $kataAsal;
        }
        
    }

    function checkPrefixDisallowedSuffixes($kata) 
    {
        // be- dan -i
        if (preg_match('/^(be)[[:alpha:]]+(i)$/i', $kata)) 
        {
            return true;
        }
        
        // di- dan -an
        if (preg_match('/^(di)[[:alpha:]]+(an)$/i', $kata)) 
        {
            return true;
        }
        
        // ke- dan -i,-kan
        if (preg_match('/^(ke)[[:alpha:]]+(i|kan)$/i', $kata)) 
        {
            return true;
        }
        
        // me- dan -an
        if (preg_match('/^(me)[[:alpha:]]+(an)$/i', $kata)) 
        {
            return true;
        }
        
        // se- dan -i,-kan
        if (preg_match('/^(se)[[:alpha:]]+(i|kan)$/i', $kata)) 
        {
            return true;
        }
        
        return FALSE;
    }

    function optimize_code()
    {
        set_time_limit(86400);
        ini_set('memory_limit', '-1');
    }

    public function spellCheck($word){
        return preg_replace_callback('/\b\w+\b/', array($this, 'spellCheckWord'),$word);
        // $pspell = pspell_new('id');
    }

    function spellCheckWord($word){
        $pspell = pspell_new('id');
        // global $pspell;
        $autocorrect = TRUE;

        $word = $word[0];
        
        
        // Ignore ALL CAPS
        if (preg_match('/^[A-Z]*$/',$word)) return $word;
    
        // Return dictionary words
        if (pspell_check($pspell,$word)) return $word;
    
        // Auto-correct with the first suggestion, color green
        if ($autocorrect && $suggestions = pspell_suggest($pspell,$word))
            return current($suggestions);
        
        // No suggestions, color red
        return $word;
    }

    public function correctWord($word){
        $word = explode(" ", $word);
        $result = "";
        foreach($word as $w){
            $result = $result." ".$this->removeRepeatedChar($w);
        }
        return trim($result);
    }

    public function removeRepeatedChar($word){
        $result = $word;
        $key = substr($word, 0, 1);
        $space = 0;

        for($i = 0 ; $i < strlen($word); $i++){
            if(strcasecmp(substr($word, $i, 1), $key) != 0){
                $replacement =" ";
                $result = substr_replace($result, $replacement, $i+$space, 0);
                $key = substr($word, $i, 1);
                $space++;
            }
        }

        $result = explode(" ", $result);
        $index = 0;
        foreach($result as $r){
            if($index == 0 || $index == count($result)-1){
                $result[$index] = substr($result[$index], 0, 1);
            }else{
                if(strlen($result[$index]) >= 2){
                    if(preg_match('/(a|i|u|e|o)/', $result[$index])){
                        $result[$index] = substr($result[$index], 0, 1);
                    }else{
                        $result[$index] = substr($result[$index], 0, 2);
                    }
                }
            }
            $index++;
        }
        return implode("", $result);
    }
}