<?php
namespace App\Classes;
use App\User;
use App\Criteria;

use App\Classes\AHP;
use Auth;

class CriteriaClass{ 
    public static function getAllCriteriaId(){
        return Criteria::where([
            ['user_id', '=', Auth::id()],
            ['criteria_type', '!=', 3]
        ])->pluck('id')->all();
    }
    public static function getAllCriteriaIdWithSA(){
        return Criteria::where('user_id', Auth::id())->pluck('id')->all();
    }

    public static function getCriteriaName($id){
        return Criteria::find($id,['criteria_name'])->criteria_name;
    }

    public function checkConsistencyRatio(){
        $ahp = new AHP();
        $criteriaId = $this->getAllCriteriaIdWithSA();
        $data = $ahp->initCriteria($criteriaId)['CR'];
        return $data;
    }
}