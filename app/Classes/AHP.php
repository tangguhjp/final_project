<?php
namespace App\Classes;

use App\CriteriaValue;
use App\Comments;
use App\Criteria;
use App\Alternative;
use App\AlternativeValue;

use Illuminate\Support\Facades\DB;
use Auth;

class AHP{
    public function process($criteria){
        $alternative = $this->initAlternative($criteria);
        
        //add sentiment criteria
        array_push($criteria, $this->getCriteriaSentimentId());

        // return $this->initCriteria($criteria);


        //get final matrix
        $alternative['final'] = $this->getFinalMatrix($alternative, $criteria);

        //get final result 
        $alternative['finalValue'] = $this->getFinalValue($alternative['final'],
                                    $this->initCriteria($criteria)['normalWeight'],
                                    $this->getAlternative($criteria));
        
        // return $alternative;    

        //sorting final value
        $sort = usort($alternative['finalValue'], function ($a, $b){
            if ($a['value']==$b['value']) return 0;
                return ($a['value']>$b['value'])?-1:1;
        });
        return $alternative;
    }

    public function initCriteria($criteria){
        $n = sizeof($criteria);
        $data = [];
        //basic matrix
        $matrix = $this->getMatrixCriteria($criteria);
        $data['matrix'] = $matrix;

        //matrix count
        $count = $this->getMatrixCount($matrix);
        $data['count'] = $count;

        //normalization matrix
        $normalizationMatrix = $this->normalizationMatrix($matrix, $count);
        $data['normalization'] = $normalizationMatrix;

        // return array_sum($normalizationMatrix[0])/5;

        //criteria weight value
        $normalWeight = $this->normalWeight($normalizationMatrix);
        $data['normalWeight'] = $this->normalWeight($normalizationMatrix);


        //get lambda
        $lambda = $this->getLambda($count, $normalWeight);
        $data['lambda'] = $lambda;

        //get consistency index
        $CI = $this->getConsistencyIndex($lambda, $n);
        $data['CI'] = $CI;

        //get consistency ratio
        $CR = $this->getConsistencyRatio($CI, $n);
        $data['CR'] = $CR;

        return $data;
    }

    public function getMatrixCriteria($criteria){
        $criteriaValue = CriteriaValue::whereIn('id_criteria', $criteria)->get();
        $nCriteria = sizeof($criteria);
        $criteriaMatrix = array();

        //get matrix value
        for($x = 0; $x < $nCriteria; $x++){
            for($y = 0; $y < $nCriteria; $y++){
                if($criteria[$x] == $criteria[$y]){
                    $criteriaMatrix[$x][$y] = 1;
                }else{
                    $data = $this->getCriteriaValue($criteria[$x], $criteria[$y]);
                    if($data){
                        $value = $data;
                    }else{
                        $value =  1 / ($this->getCriteriaValue($criteria[$y], $criteria[$x]) ?: 1);
                    }
                    $criteriaMatrix[$x][$y] = number_format($value, 6);
                }
            }
        }
        return $criteriaMatrix;
    }
    // get value of criteria comparison pair
    public static function getCriteriaValue($criteria, $criteria_comp){
        $data = CriteriaValue::where([
            ['id_criteria', '=', $criteria],
            ['id_criteria_compared', '=', $criteria_comp]
        ])->get()->first();
        if($data)
            return $data->value;
        return false;
    }

    // get count of each matrix column
    public static function getMatrixCount($matrix){
        $n = sizeof($matrix);
        $count = [];
        for($x = 0; $x < $n; $x++ ){
            $value = 0;
            for($y = 0; $y < $n; $y++ ){
                $value += $matrix[$y][$x];
            }
            array_push($count, number_format($value, 6));
        }
        return $count;
    }

    // get normalization matrix
    public static function normalizationMatrix($matrix, $count){
        $n = sizeof($matrix);
        for($x = 0; $x < $n; $x++ ){
            for($y = 0; $y < $n; $y++ ){
                $matrix[$x][$y] = number_format($matrix[$x][$y] / $count[$y], 6);
            }
        }
        return $matrix;
    }

    // get normal weight or average of each row in normalization matrix
    public static function normalWeight($matrix){
        $n = sizeof($matrix);
        $data = [];
        for($x = 0; $x < $n; $x++ ){
            array_push($data, number_format(array_sum($matrix[$x]) / $n, 6));
        }
        return $data;
    }

    // get lambda
    public static function getLambda($matrixCount, $normalWeight){
        $n = sizeof($matrixCount);
        $value = 0;
        for($x = 0;$x < $n; $x++){
            $value += ($matrixCount[$x] * $normalWeight[$x]);
        }
        return number_format($value, 6);
    }

    // get consistency index of pair comparison
    public static function getConsistencyIndex($lambda , $n){
        return number_format( ($lambda-($n))/ (($n-1) ?: 1) , 6 );
    }

    // get consistency ratio of pair comparison
    public function getConsistencyRatio($ci, $n){
        $RI = array(0, 0, 5.8, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 
            1.51, 1.48, 1.56, 1.57, 1.59);
        return number_format( $ci / (($RI[$n-1]) ?: 1)  , 6 );        
    }
    
    public function initAlternative($criteria){
        $alternative  = $this->getMatrixAlternative($criteria);
        
        for($i = 0; $i < count($alternative);$i++){
            $matrix = $alternative[$i]['matrix'];
            $n = count($matrix);
            $matrixCount = $this->getMatrixCount($matrix);
            $normalization = $this->normalizationMatrix($matrix, $matrixCount);
            $normalWeightAlternative = $this->normalWeight($normalization);
            $lambda = $this->getLambda($matrixCount, $normalWeightAlternative);

            $alternative[$i]['normalization'] = $normalization;
            $alternative[$i]['normalWeight'] = $normalWeightAlternative;
            $alternative[$i]['lambda'] = $lambda;
            $alternative[$i]['CI'] = $this->getConsistencyIndex($lambda, $n);
            // $alternative[$i]['CR'] = $this->getConsistencyRatio($alternative[$i]['CI'], $n);
        }
        return $alternative;
    }
    // get matrix alternative
    public function getMatrixAlternative($criteria){
        $alternative = $this->getAlternative($criteria);
        $n = count($alternative);
        $alternativeMatrix = [];

        for($i = 0; $i < count($criteria); $i++){
            $matrix = array();
            for($x = 0; $x < $n; $x++){
                for($y = 0; $y < $n; $y++){
                    if($x == $y){
                        $matrix[$x][$y] = 1;
                    }else{
                        $data = $this->getAlternativeValue($criteria[$i], 
                            $alternative[$x], $alternative[$y]);
                        if($data){
                            $value = $data;
                        }else{
                            $value = 1 / ($this->getAlternativeValue($criteria[$i], 
                                $alternative[$y], $alternative[$x]) ?: 1);
                        }
                        $matrix[$x][$y] = number_format($value, 6);                          
                    }
                }
            }
            $dataMatrix = [];
            $dataMatrix['matrix'] = $matrix;
            array_push($alternativeMatrix, $dataMatrix);
        }
        return $alternativeMatrix;
    }

    // get alternative ID
    public static function getAlternative($criteria){
        return $alternative = AlternativeValue::select('id_alternative')
            ->where('id_criteria', $criteria[0])->distinct()->pluck('id_alternative');
    }

    // get alternative value in pair comparison
    public function getAlternativeValue($criteria, $altern, $altern_compared){
        $data = AlternativeValue::where([
            ['id_criteria', '=', $criteria],
            ['id_alternative', '=', $altern],
            ['id_alternative_compared', '=', $altern_compared],
        ])->get()->first();
        if($data){
            if($this->checkNormalCriteria($criteria)){
                return $data->value;
            }
            return  1 / ($data->value ?: 1) ;
        }
        return false;
    }

    public function checkNormalCriteria($id){
        $data = Criteria::find($id);
        if($data->criteria_type != 1){
            return true;
        }return false;
    } 

    // get final matrix from all normal weight of alternative in each criteria
    public function getFinalMatrix($matrixAlternative, $criteria){
        $matrix = array();
        for($x = 0; $x < count($matrixAlternative); $x++){
            $alternativeWeight = $matrixAlternative[$x]['normalWeight'];
            $nAlternative = count($alternativeWeight);
            for($y = 0; $y < $nAlternative; $y++){
                $matrix[$y][$x] = $alternativeWeight[$y];
            }   
        }

        $alternative = $this->getAlternative($criteria);
        $minimumComments = $this->getMinimumComment($alternative);
        for($i = 0; $i < count($matrix); $i++){
            $lastIndex = count($matrix[$i]);
            $matrix[$i][$lastIndex] = $this->getSentimentScore($alternative[$i], $minimumComments);
        }
        return $matrix;
    }

    // get result from AHP
    public static function getFinalValue($finalMatrix, $criteriaWeight, $alternativeId){
        $finalResult = [];
        for($x = 0; $x < count($finalMatrix); $x++){
            $data = [];
            $value = 0;
            for($y = 0; $y < count($criteriaWeight); $y++){
                $value += $finalMatrix[$x][$y] * $criteriaWeight[$y];
            }
            $data['alternative_id'] = $alternativeId[$x];
            $data['value'] = number_format($value, 6);
            $finalResult[] = $data;
        }
        return $finalResult;
    }

    public static function getCriteriaSentimentId(){
        $data = Criteria::where([
            ['user_id', '=', Auth::id()],
            ['criteria_type', '=', 3]
        ])->get()->first();
        return $data->id;
    }

    public static function getSentimentScore($alternativeId, $n){
        // $n = 10;
        // $data = Comments::select('id_alternative', 
        //         DB::raw('SUM( (CASE WHEN value = 1 THEN 1 END) ) AS positive'),
        //         DB::raw('SUM( (CASE WHEN value = -1 THEN 1 END) ) AS negative'),
        //         DB::raw('SUM( (CASE WHEN value = 0 THEN 1 END) ) AS netral')
        //     )->where('id_alternative', $alternativeId)->groupBy('id_alternative')->get()->first();
        // $data = json_decode(json_encode($data), true);
        $data = Comments::select('value')->where('id_alternative', $alternativeId)->take($n)->get();
        $temp = 0;
        foreach($data as $d){
            $temp += $d['value'];
        }
        return number_format($temp / (($n) ?: 1), 6);

        // return $data;   
        // $score = ($data['positive']-$data['negative']) / 
        //     (($data['positive']+$data['negative']+$data['netral']) ?: 1);
        // return number_format($score, 6);
    }

    public static function getMinimumComment($alternative){
        $result = 20;
        foreach($alternative as $alt){
            $data = Comments::select('value')->where('id_alternative', $alt)->get();
            if(count($data) < $result && count($data) != 0){
                $result = count($data);
            }
        }
        return $result;
    }
}