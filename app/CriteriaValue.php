<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CriteriaValue extends Model
{
    protected $table = 'criteria_value';
    public $timestamps = false;
}
