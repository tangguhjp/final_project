<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardWords extends Model
{
    protected $table = 'kata_baku';
    public $timestamps = false;
}
