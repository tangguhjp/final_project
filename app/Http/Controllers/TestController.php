<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Criteria;
use App\Alternative;
use App\CriteriaValue;
use App\AlternativeValue;
use App\BasicWords;
use App\Classes\CriteriaClass;
use App\Classes\AlternativeClass;
use App\Classes\SocialMedia;
use App\Classes\SentimentAnalysis;
use App\Classes\TextMining;
use Illuminate\Support\Facades\DB;
use Abraham\TwitterOAuth\TwitterOAuth;  
use App\Classes\AHP;
 
use Illuminate\Http\Request;
use Auth;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $AHP ;

    public function __construct()
    {
        // $this->middleware('auth');
        // $this->AHP = new AHP();
    }

    public function index()
    {
        $alternative[] = 154;
        $alternative[] = 153;
        $alternative[] = 152;
        $ahp = new AHP();
        return $ahp->getMinimumComment($alternative);

        $id[] = 166;
        $ahpResult = $ahp->process($id);
        return $ahpResult;
    }

    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = $this->AHP->process($request->criteria);
        // // return $data;
        // return response()->json($request->all());
        

        $SA = new SentimentAnalysis();
        $value = $SA->nazief($request->keyword);
        return $value;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new \StdClass();
        // $data = new \StdClass();
        // $request->setAttribute('id', $id);
        $data = (object)$request->all();
        $data->id = $id;
        // $data->setAttribute('id', $id);
        $result->data = $data;
        $result->status = 200;

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = new \StdClass();
        $result->status = 200;
        $result->message = 'sukses';
        // $result->status = 200;
        return response()->json($result);
    }

    public function addCriteria(Request $request){
        $data = new Criteria();
        $data->user_id = Auth::id();
        $data->criteria_name = $request->criteria_name; 
        $data->criteria_type = $request->criteria_type; 
        $data->setAttribute('id', 10);
        $value = new \StdClass();
        $value->status = 10;
        $value->data = $data;
        $value->message = "tidak dapat upload file";
        return response()->json($value);
    }

    public function getPost(Request $request){
        $medsos = new SocialMedia ;
        $medsos = $medsos->getTwitterPost($request->keyword);
        return response()->json($medsos);
    }
 
    public function getSentiment(Request $request){
        $SA = new SentimentAnalysis();
        $value = $SA->process($request->comment);
        return $value;
    }

    public function correctWord(Request $request){
        $word = $this->spellCheck($request->word);
        $TM = new TextMining();
        return $TM->spellCheck($word);
    }

    public function spellCheck($word){
        $word = explode(" ", $word);
        $result = "";
        foreach($word as $w){
            $result = $result." ".$this->removeRepeatedChar($w);
        }
        return trim($result);
    }

    public function removeRepeatedChar($word){
        $result = $word;
        $key = substr($word, 0, 1);
        $space = 0;

        for($i = 0 ; $i < strlen($word); $i++){
            if(strcasecmp(substr($word, $i, 1), $key) != 0){
                $replacement =" ";
                $result = substr_replace($result, $replacement, $i+$space, 0);
                $key = substr($word, $i, 1);
                $space++;
            }
        }

        $result = explode(" ", $result);
        $index = 0;
        foreach($result as $r){
            if($index == 0 || $index == count($result)-1){
                $result[$index] = substr($result[$index], 0, 1);
            }else{
                if(strlen($result[$index]) >= 2){
                    if(preg_match('/(a|i|u|e|o)/', $result[$index])){
                        $result[$index] = substr($result[$index], 0, 1);
                    }else{
                        $result[$index] = substr($result[$index], 0, 2);
                    }
                }
            }
            $index++;
        }
        return implode("", $result);
    }
}
