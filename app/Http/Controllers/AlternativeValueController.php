<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlternativeValue;
use App\Criteria;
use App\Alternative;
use App\Preferences;

use App\Classes\AlternativeClass;
use App\Classes\CriteriaClass;
use App\Classes\AHP;
use Auth;
use Session;

class AlternativeValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $CRITERIA_CLASS;
    private $ALTERNATIVE_CLASS;
    private $AHP;

    public function __construct()
    {
        $this->middleware('auth');
        $this->CRITERIA_CLASS = new CriteriaClass;
        $this->ALTERNATIVE_CLASS = new AlternativeClass;
        $this->AHP = new AHP;
    }

    public function index()
    {
        if(!$this->checkAlternativeExists()){
            Session::flash('alert', 'Add your alternative!');
            return redirect('alternative');
        }else if(!$this->checkHaveCriteria()){
            Session::flash('alert', 'Add your criteria!');
            return redirect('criteria');
        }
        $criteria = $this->CRITERIA_CLASS->getAllCriteriaId();
        $data = [];
        foreach($criteria as $c){
            $alternative['criteria_id'] = $c;
            $alternative['criteria_name'] = $this->CRITERIA_CLASS->getCriteriaName($c);
            $alternative['data'] = AlternativeValue::
                join('alternative AS altern', 'alternative_value.id_alternative', 'altern.id')
                ->join('alternative AS altern_comp', 'alternative_value.id_alternative_compared', 
                'altern_comp.id')
                ->select(
                    'alternative_value.id',
                    'alternative_value.value',
                    'altern.name AS altern_name', 
                    'altern_comp.name AS altern_name_compared')
                ->where([
                    ['alternative_value.id_criteria', '=', $c],
                    ['alternative_value.value', '!=', '1']
                    ])->get();
            
            $alternative['weight'] = $this->getAlternativeWeight($c);
            array_push($data, $alternative);
        }
        if($data[0]['weight'] < 2){
            Session::flash('alert', 'Add your alternative!');
            return redirect('alternative');
        }
        $preferences = Preferences::orderBy('value', 'asc')->get();
        $sentimentWeight = $this->getSentimentWeight();

        return view('alternative-value', compact('data', 'preferences', 'sentimentWeight'));
    }

    public function checkAlternativeExists(){
        if(Alternative::where('id_user', Auth::id())->exists()){
            return true;
        }
        return false;
    }

    public function checkHaveCriteria(){
        if(Criteria::where([ 
            ['user_id', '=' , Auth::id()],
            ['criteria_type', '!=', 3]])->exists()){
                return true;
        }else{
            return false;
        }
    }

    public function getAlternativeWeight($criteria){
        $alternativeId = $this->ALTERNATIVE_CLASS->getAllAlternativeId();
        $weight = $this->AHP->initAlternative(array($criteria))[0]['normalWeight'];
        $alternativeWeightData = [];
        $i = 0;
        foreach($weight as $w){
            $alternData = [];
            $alternData['alternative_name'] = $this->ALTERNATIVE_CLASS->getAlternativeName($alternativeId[$i]);
            $alternData['weight'] = $w;
            array_push($alternativeWeightData, $alternData);
            $i++;
        }
        usort($alternativeWeightData, function ($a, $b){
            if ($a['weight']==$b['weight']) return 0;
                return ($a['weight']>$b['weight'])?-1:1;
        });
        return $alternativeWeightData;
    }

    public function getSentimentWeight(){
        $data = [];
        $minimumComments = $this->AHP->getMinimumComment($this->ALTERNATIVE_CLASS->getAllAlternativeId());
        foreach($this->ALTERNATIVE_CLASS->getAllAlternativeId() as $id){
            $alternative = [];
            $alternative['alternative_name'] = $this->ALTERNATIVE_CLASS->getAlternativeName($id);
            $alternative['score'] = $this->AHP->getSentimentScore($id, $minimumComments);
            array_push($data, $alternative);
        }
        usort($data, function ($a, $b){
            if ($a['score']==$b['score']) return 0;
                return ($a['score']>$b['score'])?-1:1;
        });
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new \StdClass();
        $result->status = 200;
        $data = AlternativeValue::find($id);
        $data->value = floatval($request->input('value'));

        if($data->save()){
            $data = (object)$request->all();
            $data->id = $id;
            $result->data = $data;
            return response()->json($result);
            Session::flash('alert-success', 'Alternative Value Updated');            
            return back();
        }else{
            Session::flash('alert', 'Failed To Save Alternative Value');            
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
