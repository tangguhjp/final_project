<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.login-form');
    }

    public function register(){
        return view('user.register-form');
    }

    public function getUsers(){
        if(Auth::user()->role_id != 2){
            return redirect()->route('home');
        }
        $user = User::all()->except(Auth::id());
        return view('users', compact('user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->username = $request->username;
        $data->role_id = $request->role_id;
        $data->password = Hash::make($request->password);
        if($data->save()){
            Session::flash('alert-success', 'User added.');
            return back();
        }else{
            Session::flash('alert', 'Failed to add user!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->role_id = $request->role_id;
        if(!empty($request->password)){
            $data->password = Hash::make($request->password);        
        }
        if($data->save()){
            Session::flash('alert-success', 'User data updated.');
            return back();
        }else{
            Session::flash('alert', 'Update user failed!');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = new \StdClass();
        $result->status = 200;

        if(User::find($id)->delete()){
            $result->message = 'User deleted';
        }else{
            $result->status = 300;
            $result->message = 'Failed to delete user';
        }
        return response()->json($result);
    }
}
