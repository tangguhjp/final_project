<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria;
use App\CriteriaValue;
use App\Preferences;
use App\Classes\AHP;
use App\Classes\CriteriaClass;

use Auth;
use Session;

class CriteriaValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private $AHP;
    private $CRITERIA;

    public function __construct()
    {
        $this->middleware('auth');
        $this->AHP = new AHP;
        $this->CRITERIA =  new CriteriaClass;
    }
    public function index()
    {
        $data = CriteriaVAlue::
            join('criteria AS c', 'criteria_value.id_criteria', 'c.id')
            ->join('criteria AS c_comp', 'criteria_value.id_criteria_compared', 'c_comp.id')
            ->select(
                'criteria_value.id',
                'criteria_value.value',
                'c.criteria_name AS c_name', 
                'c_comp.criteria_name AS c_name_compared')
            ->where([
                ['c.user_id', '=', Auth::id()],
                ['criteria_value.value', '!=', '1']
                ])->get();
        if(count($data) < 2){
            Session::flash('alert', 'You must add more criteria!');
            return redirect('criteria');
        }
        $preferences = Preferences::orderBy('value', 'asc')->get();
        $weight = $this->getCriteriaWeight();
        $CR = $this->CRITERIA->checkConsistencyRatio();
        return view('criteria-value', compact('data','preferences', 'weight', 'CR'));
    }

    public function getCriteriaWeight(){
        $criteria = Criteria::where('user_id', Auth::id())->pluck('id');
        $criteriaWeight = $this->AHP->initCriteria($criteria)['normalWeight'];
        $result = [];
        $i = 0;
        foreach($criteria as $c){
            $data = [];
            $data['criteria_name'] = $this->CRITERIA->getCriteriaName($c);
            $data['weight'] = $criteriaWeight[$i];
            array_push($result, $data);
            $i++;
        }

        usort($result, function ($a, $b){
            if ($a['weight']==$b['weight']) return 0;
                return ($a['weight']>$b['weight'])?-1:1;
        });

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->input('user_id');
        $id_criteria = Criteria::where('user_id', '=', $user_id)->pluck('id')->all();
        return $id_criteria[0];
    }
    public function getUserCriteria($user_id){
        return Criteria::where('user_id', '=', $user_id)->pluck('id')->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new \StdClass();
        $result->status = 200;

        $data = CriteriaValue::find($id);
        $data->value = floatval($request->input('value'));

        if($data->save()){
            $data = (object)$request->all();
            $data->id = $id;
            $result->data = $data;
            $result->cr = $this->CRITERIA->checkConsistencyRatio();
        }else{
            $result->status = 300;
            $result->message = 'Failed to update value of criteria comparison.';
        }
        return response()->json($result);

        $result = new \StdClass();
        // $data = new \StdClass();
        // $request->setAttribute('id', $id);
        $data = (object)$request->all();
        $data->id = $id;
        // $data->setAttribute('id', $id);
        $result->data = $data;
        $result->status = 200;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
