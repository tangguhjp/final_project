<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Classes\CriteriaClass;
use App\Classes\AlternativeClass;

use App\Criteria;
use App\CriteriaValue;
use App\AlternativeValue;
use App\Goals;
use App\User;

use Redirect;
use Session;

class CriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $WEB_IDENTIFIER = 'web';
    private $CRITERIA_CLASS ;
    private $ALTERNATIVE_CLASS ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->CRITERIA_CLASS =  new CriteriaClass();
        $this->ALTERNATIVE_CLASS =  new AlternativeClass();
    }

    public function index()
    {
        // return $this->CRITERIA_CLASS->getAllCriteriaId();
        $id = Auth::id();
        $data = Criteria::where('user_id', $id)->get()->all();
        $this->initCriteriaInAlternative();
        return view('criteria', compact('data'));
    }

    public function userCriteria($user_id){
        $data = Criteria::where('user_id', $user_id)->get()->all();
        return $data;
    }

    public function critest(Request $request, $test){
        return $request;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    public function initCriteriaInAlternative(){ 
        // get all criteria Id of user
        $userCriteria = $this->CRITERIA_CLASS->getAllCriteriaId();
        foreach($userCriteria as $criteria){
            // check criteria init in alternative value
            if(!AlternativeValue::where('id_criteria',$criteria)->first()){
                $altern = $this->ALTERNATIVE_CLASS->getAllAlternativeId();
                $altern_compared = $altern;
                $alternativeValueData = [];
                foreach($altern as $alt){
                    foreach((array)$altern_compared as $alt_compare){
                        $data = [];;
                        $data['id_criteria'] = $criteria;
                        $data['id_alternative'] = $alt;
                        $data['id_alternative_compared'] = $alt_compare;
                        $data['value'] = ($alt == $alt_compare) ? 1 : 0;
                        array_push($alternativeValueData, $data);
                    }
                    array_shift($altern_compared);
                }
                $this->inputCompareAlternative($alternativeValueData);
            }
        }
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $criteria = $request->criteria_name;
        $criteria_type = $request->criteria_type;
        $identifier = $request->identifier;

        $result = new \StdClass();
        $result->status = 200;

        if(User::find(Auth::id())){
            if(!Goals::where('user_id', Auth::id())->exists()){
                $result->status = 301;
                $result->message = "Please input your goal!";
                return response()->json($result);   
            }
            if(sizeof(Criteria::where('user_id', Auth::id())->get()) < 7){
                if($this->criteriaCheck($criteria)){
                    $result->status = 300;
                    $result->message = "The Criteria Already Exist";
                }else{
                    if(!$this->sentimentExist()){
                        if(!$this->createSentimentCriteria()){
                            Session::flash('alert', 'Failed Create Sentiment Criteria');
                            return back();
                        }
                    }
                    $data = new Criteria();
                    $data->user_id = Auth::id();
                    $data->criteria_name = $criteria; 
                    $data->criteria_type = $criteria_type; 

                    if($data->save()){
                        $this->storeCriteriaValue($data->id);
                        $data->setAttribute('id', $data->id);
                        $result->message = "Criteria added.";
                        $result->data = $data;
                        $this->initCriteriaInAlternative();
                    }else{
                        $result->status = 300;
                        $result->message = 'Failed to save criteria';
                    }
                }
            }else{
                $result->status = 300;
                $result->message = "You are only allowed to enter 7 criteria";
            }
        }else{
            $result->status = 300;
            $result->message = "User ID not registered";
        }
        return response()->json($result);
    }

    public function sentimentExist(){
        if(Criteria::where('user_id', Auth::id())
            ->where('criteria_type', '3')->exists()){
                return true;
            }
            return false;
    }

    public function createSentimentCriteria(){
        $data = new Criteria;
        $data->user_id = Auth::id();
        $data->criteria_name = 'Sentiment Analysis'; 
        $data->criteria_type = 3; 

        if($data->save()){
            $this->storeCriteriaValue($data->id);
            return true;
        }
        return false;
    }
    public function criteriaCheck($criteria){
        if(Criteria::where('user_id', Auth::id())
            ->where('criteria_name', $criteria)->first()){
            return true;
        }else{
            return false;
        }
    }

    
    //add comparing criteria in table criteria_value
    public function storeCriteriaValue($id){
        $userCriteria = $this->CRITERIA_CLASS->getAllCriteriaIdWithSA();
        $criteriaValue = [];
        foreach($userCriteria as $criteriaCompared){
            $data = [];
            $data['id_criteria'] = $id;
            $data['id_criteria_compared'] = $criteriaCompared;
            $data['value'] = ($id == $criteriaCompared) ? 1 : 0;
            array_push($criteriaValue, $data);
        }
        $this->inputComparingCriteria($criteriaValue);
    }

    public function inputComparingCriteria($data){
        if(CriteriaValue::insert($data)){
            return true;
        }else{
            return false;
        }
    }

    public function inputCompareAlternative($data){
        if(AlternativeValue::insert($data)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new \StdClass();

        $result->status = 200;
        $data = Criteria::find($id);
        $data->criteria_name = $request->criteria_name;

        if($data->save()){
            $result->data = $data;
        }else{
            $result->status = 300;
            $result->message = 'Failed to update criteria.';
        }
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $identifier = $request->identifier;
        $result = new \StdClass();
        $result->status = 200;

        if(Criteria::find($id)->delete()){
            $result->message = 'Criteria Deleted';
        }else{
            $result->status = 300;
            $result->message = 'Failed to delete criteria';
        }
        return response()->json($result);
    }
}
