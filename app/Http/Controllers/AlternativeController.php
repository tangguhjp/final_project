<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Classes\AlternativeClass;
use App\Classes\CriteriaClass;
use App\Classes\SentimentAnalysis;
use App\Classes\SocialMedia;

use App\Alternative;
use App\AlternativeValue;
use App\Criteria;
use App\Comments;
use App\Goals;
use App\User;

use Auth;
use Session;

class AlternativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $WEB_IDENTIFIER = 'web';
    private $CRITERIA_CLASS;
    private $ALTERNATIVE_CLASS;

    public function __construct()
    {
        $this->middleware('auth');
        $this->CRITERIA_CLASS = new CriteriaClass();
        $this->ALTERNATIVE_CLASS = new AlternativeClass();
    }

    public function index()
    {
        $data = Alternative::where('id_user', Auth::id())->get()->all();
        return view('alternative', compact('data'));
    }
    public function userAlternative($user_id){
        return Alternative::where('id_user', $user_id)->get()->all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new \StdClass();
        $result->status = 200;
        $id_user = $request->input('user_id');

        if(User::find($id_user)){
            if(!Goals::where('user_id', Auth::id())->exists()){
                $result->status = 300;
                $result->message = "Add your recommendation goal!";
                return response()->json($result);
            }

            if(!$this->checkAlternativeExist($request->input('alternative_name'))){
                $connection = $this->checkConnection();
                if($connection->status != 200){
                    $result->status = 300;
                    $result->message = $connection->message;
                    return response()->json($result);
                }
                $data = new Alternative();
                $data->id_user = Auth::id();
                $data->name = $request->input('alternative_name');
                $data->info = $request->input('alternative_info');
                if($data->save()){
                    $this->storeAlternativeValue($data->id);
                    $result->data = $data;
                    $result->message = "Alternative added.";

                    $this->getMedsosPost($data);
                }else{
                    Session::flash('alert', 'Failed to save alternative data.');
                    return back();
                }
            }else{
                $result->status = 300;
                $result->message = "Alternative already exist.";
            }
            return response()->json($result);
        }
    }

    public function checkAlternativeExist($name){
        if(Alternative::where('id_user', Auth::id())
            ->where('name', $name)->first()){
            return true;
        }else{
            return false;
        }
    }

    public function storeAlternativeValue($curr_altern_id){
        $userCriteria = $this->CRITERIA_CLASS->getAllCriteriaId();
        $userAlternative = $this->ALTERNATIVE_CLASS->getAllAlternativeId();  
        
        $alternativeValueData = [];
        foreach($userCriteria as $criteria){
            foreach ($userAlternative as $altern) {
                $data = [];
                $data['id_criteria'] = $criteria;
                $data['id_alternative'] = $curr_altern_id;
                $data['id_alternative_compared'] = $altern;
                $data['value'] = ($curr_altern_id == $altern) ? 1 : 0;
                array_push($alternativeValueData, $data);
            }
        }
        if(!$this->inputCompareAlternative($alternativeValueData)){
            return "FAILED inputComparingAlternative";
        }
    }

    public function inputCompareAlternative($data){
        return (AlternativeValue::insert($data)) ? true : false;
    }

    public function getMedsosPost($alternative){
        $post = new SocialMedia ;
        $post = $post->getTwitterPost($alternative->name);
        if($post->status == 200){
            foreach($post->data as $post){
                $data = [];
                $data['id_alternative'] = $alternative->id;
                $data['user'] = $post->user;
                $data['comment'] = $post->text;
                $data['comment_date'] = $post->time;
                $data['value'] = $this->analyzePost($post->text);
                
                $allPost[] = $data;
            }
            Comments::insert($allPost);
        }
    }

    public function analyzePost($post){
        $SA = new SentimentAnalysis();
        $value = $SA->process($post);
        return ($value['score'] < 0 ? -1 : ($value['score'] > 0 ? 1 : 0));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $data = new \StdClass();
        $data->alternative = Alternative::where([
            ['id', '=', $id],
            ['id_user', '=', Auth::id()]
        ])->get()->first();

        if(empty($data->alternative)){
            return "Data not found";
        }

        $data->comments = Comments::where('id_alternative', $id)->get();
        return view('alternative-details', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $result = new \StdClass();
        $result->status = 200;

        $data = Alternative::find($id);
        $data->name = $request->input('alternative_name');
        $data->info = $request->input('alternative_info');

        if($data->save()){
            $result->data = $data;
            $result->message = "Alternative updated.";
        }else{
            $result->status = 300;
            $result->message = "Failed to update alternative.";
        }
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $result = new \StdClass();
        $result->status = 200;
        if(Alternative::find($id)->delete()){
            $result->message = "Alternative deleted.";
        }else{
            $result->status = 300;
            $result->message = "Failed to delete alternative.";
        }
        return response()->json($result);
    }


    public function checkConnection(){
        $result = new \StdClass();
        $result->status = 200;
        $fp = @fsockopen("api.twitter.com",443);
        if (!$fp) {
            $result->status = 300;
            $result->message = "No internet, please check your connection!";
        } else {
            fwrite($fp, "GET / HTTP/1.0\r\n\r\n");
            stream_set_timeout($fp, 2);
            $res = fread($fp, 500);

            $info = stream_get_meta_data($fp);
            fclose($fp);

            if ($info['timed_out']) {
                $result->status = 300;
                $result->message = "Request time out!";
            }
        }
        return $result;
    }
}
