<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria;
use App\Alternative;
use App\Classes\AHP;
use App\Classes\AlternativeClass;
use App\Classes\CriteriaClass;
use App\Goals;

use Auth;
use Session;

class CountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $AHP ;
    private $ALTERNATIVE_CLASS;
    private $CRITERIA_CLASS;

    public function __construct()
    {
        // $this->middleware('auth');
        $this->AHP = new AHP();
        $this->ALTERNATIVE_CLASS = new AlternativeClass();
        $this->CRITERIA_CLASS = new CriteriaClass();
    }
    public function index()
    {
        $criteria = Criteria::where([
            ['user_id', '=', Auth::id()],
            ['criteria_type', '!=', 3]
        ])->get();

        if(count($criteria) < 2){
            Session::flash('alert', 'Please add at least 2 criteria!');
            return redirect('criteria');
        }else if($this->CRITERIA_CLASS->checkConsistencyRatio() >= 0.1){
            Session::flash('alert', 'Comparison matrix is inconsistent. Please input again your criteria value!');
            return redirect('criteria-value');
        }
        $goal = $this->getGoal();
        return view('count', compact('criteria', 'goal'));
    }

    public function getGoal(){
        $data = Goals::where('user_id', Auth::id())->get()->first();
        if($data)
            return $data->goal;
        return '-';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->criteria) < 1){
            Session::flash('alert', 'You must choose at least 2 criteria');
            return back();
        }else if($this->checkCriteriaValue()){
            Session::flash('alert', 'Please check criteria value! There is 0 value in comparison of criteria.');
            return back();
        }else if($this->checkAlternativeValue()){
            Session::flash('alert', 'Please check alternative value! There is 0 value in comparison of alternative.');
            return back();
        }else if(count($this->ALTERNATIVE_CLASS->getAllAlternativeId()) < 2){
            Session::flash('alert', 'Please add at least 2 alternatives!');
            return redirect('alternative');
        }

        $ahpResult = $this->AHP->process($request->criteria)['finalValue'];
        $data = [];
        foreach($ahpResult as $d){
            $alternative = Alternative::find($d['alternative_id']);
            $alternative['score'] = $d['value'];
            array_push($data, $alternative);
        }
        Session::put('history', $data);
        $goal = $this->getGoal();
        return view('count-result', compact('data', 'goal'));
    }

    public function checkCriteriaValue(){
        $criteria = Criteria::join('criteria_value', 'criteria_value.id_criteria', 'criteria.id')
            ->select('criteria.*')->where([
                ['criteria.user_id', '=', Auth::id()],
                ['criteria_value.value', '=', 0]
            ])->exists();
        if($criteria)
            return true;
        return false;
    }

    public function checkAlternativeValue(){
        $alternative = Alternative::join('alternative_value', 'alternative_value.id_alternative', 'alternative.id')
            ->join('criteria', 'criteria.id', 'alternative_value.id_criteria')
            ->select('alternative.*')->where([
                ['alternative.id_user', '=', Auth::id()],
                ['alternative_value.value', '=', 0]
            ])->exists();
        if($alternative)
            return true;
        return false;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
