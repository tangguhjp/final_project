<?php

namespace App\Http\Controllers;
use App\Goals;
use Auth;
use Session;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Goals::where('user_id', Auth::id())->get()->first();
        if($data){
            $goal = $data->goal;
        }else{
            $goal = '-';
        }

        return view('dashboard', compact('goal'));
    }

    public function manageGoal(Request $request){
        if(is_null($request->goal)){
            Session::flash('alert', 'The goal should not be empty!');
            return back();
        }
        if(Goals::where('user_id', Auth::id())->exists()){
            Goals::where('user_id', Auth::id())->update([
                'goal' => $request->goal
            ]);
        }else{
            $goal = new Goals;
            $goal->user_id = Auth::id();
            $goal->goal = $request->goal;
            $goal->save();
            Session::put('goal', $goal->goal);
        }

        return back();
    }
}
