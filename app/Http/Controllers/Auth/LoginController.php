<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use Illuminate\Http\Request;

use App\Goals;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->redirectTo = route('criteria.index');
    }
    
    public function username()
    {
        return 'username';
    }

    public function showLoginForm(){
        return view('user.login-form');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }

    protected function authenticated(Request $request, $user)
    {
        $data = Goals::where('user_id', Auth::id())->get()->first();
        if($data){
            $goal = $data->goal;
        }else{
            $goal = '-';
        }
        Session::put('goal', $goal);
    }
}
