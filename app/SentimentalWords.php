<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentimentalWords extends Model
{
    protected $table = 'sentimental_word';
    public $timestamps = false;
}
