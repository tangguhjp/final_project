<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stoplists extends Model
{
    protected $table = 'stoplist';
    public $timestamps = false;
}
