<div class="modal-header">						
    <h4 id="alternative-modal-title" class="modal-title">Add Alternative</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">					
    <div class="form-group">
        <label>Alternative Name</label>
        <input type="text" class="form-control" name="name" id="alt_name" required>
    </div>
    <div class="form-group">
        <label>Alternative Information</label>
        <textarea type="text" class="form-control" name="info" id="alt_info" rows="3" required></textarea>
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" name="action" id="alternative-action" value="add">
        <input type="hidden" class="form-control" name="alt_id" id="alt_id">
        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
    </div>					
</div>