@extends('main-template', [
  'title' => 'User', 
  'activePage'=>'manage-user',
  'activeChild'=>'none'
  ])
@section('content')

  <div class="table-wrapper">
    <div class="table-title">
        <div class="d-flex">
            <div class="mr-auto p-2">
              <h2><b>Users</b> Data</h2>
            </div>
            <div class="ml-auto p-2">
              <a id="add-user" href="#" class="btn btn-success" data-toggle="modal" data-target="#manage-users" >
                <i class="material-icons">&#xE147;</i> 
                <span class="span-add" >Add User</span>
              </a>  
            </div>
        </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-hover table-text-center" id="table-manage-users">
          <thead>
              <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach($user as $u)
              <tr id="u-row-{{$u->id}}">
                  <td></td>
                  <td>{{$u->name}}</td>
                  <td>{{$u->username}}</td>
                  <td>{{$u->email}}</td>
                  <td>{{$u->password}}</td>
                  <td>
                      <a href="#" class="edit edit-user" 
                        data-id="{{$u->id}}" data-toggle="modal" data-target="#manage-users">
                        <i class="material-icons" data-toggle="tooltip">&#xE254;</i>
                      </a>
                      <a href="#delete_modal" class="delete" data-toggle="modal" data-id="{{$u->id}}">
                        <i class="material-icons" data-toggle="tooltip">&#xE872;</i>
                      </a>
                  </td>
              </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
@endsection