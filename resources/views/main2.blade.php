<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sycom Corp</title>

        <!-- Bootstrap CSS CDN -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css') }}/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css') }}/bootstrap/awesome-bootstrap-checkbox.css">
        <link rel="stylesheet" href="{{ asset('main') }}/css/googleapis.css">
        <link rel="stylesheet" href="{{ asset('main') }}/css/googleapis-material-icon.css">
        <link rel="stylesheet" href="{{ asset('main') }}/css/font-awesome.min.css?v=3.2.1">
        <link rel="stylesheet" href="{{ asset('main') }}/css/bootstrap.css">
        <link rel="stylesheet" href="{{ asset('main') }}/css/dataTables.bootstrap4.min.css">
            <!-- Compiled and minified CSS -->

        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css') }}/custom_style.css">
    </head>

    <body onload="myFunction()">
        <div class="btn" onclick="topFunction()" id="btn-top">
            <b>Top</b>
        </div>
        <div id="loader"></div>
        @include('sidebar') 

        <script defer src="{{ asset('main') }}/js/jquery.min.js"></script>
        <script defer src="{{ asset('main') }}/js/jquerysession.js"></script>

        <!-- dataTables -->
        <script defer src="{{ asset('main') }}/js/jquery-3.3.1.js"></script>
        <script defer src="{{ asset('main') }}/js/jquery.dataTables.min.js"></script>
        <script defer src="{{ asset('main') }}/js/dataTables.bootstrap4.min.js"></script>

        <script src="{{ asset('main') }}/js/jquery-3.3.1.slim.min.js"></script>
        <script src="{{ asset('main') }}/js/popper.min.js"></script>
        <script defer src="{{ asset('main') }}/js/solid.js"></script>
        <script defer src="{{ asset('main') }}/js/fontawesome.js"></script>
        <script src="{{ asset('main') }}/js/bootstrap.min.js"></script>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <script>
            var activePage = "<?= $activePage ?>";
        </script>
        
        <!-- custom js -->
        <script src="{{ asset('main') }}/js/app.js"></script>
        <script src="{{ asset('main') }}/js/custom.js"></script>
    </body>
</html>