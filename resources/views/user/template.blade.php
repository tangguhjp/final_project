<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Welcom to Sycom Corp.</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style type="text/css">
    body{
        background-image: url("/image/BG_web_white.png");
    }
    .login-form {
		width: 340px;
        margin: 12% auto;
    }
    
    .register-form{
        margin: 5% auto;
    }
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 45px;
        border-radius: 5px;
    }
    
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
        background-color: #016a87;
    }
    .login-form a{
        color: #016a8766;
    }

    .login-form h2{
        color: #016a87;
    }
    @media (max-width: 360px) {
        .login-form{
            width: auto;
            margin: 45% 10% 0% 10%;
        }
        .login-form form{
            padding: 15px;
        }
    }
</style>
</head>
<body>
    @yield('content')
</body>
</html>                                		                            