@extends('user.template')
@section('content')
<div class="login-form">
    <form action="{{route('login')}}" method="post">
        @csrf
        <h2 class="text-center">LOG IN</h2>       
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
    <p class="text-center"><a href="{{route('user-register')}}">Create an Account</a></p>
    </form>
</div>
@endsection