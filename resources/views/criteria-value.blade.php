@extends('main-template', [
  'title' => 'Criteria Value', 
  'activePage'=>'criteria',
  'activeChild'=>'cc-criteria'
  ])
@section('content')
  <div class="table-wrapper">
    <div class="table-title">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <h2><b>Criteria Value</b> Data</h2>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-hover table-text-center long-table">
        <thead>
          <tr>
            <th>No.</th>
            <th>Criteria</th>
            <th>Comparison Criteria</th>
            <th>Value</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $d)
          <tr id="cv-row-{{$d->id}}">
              <td></td>
              <td>{{$d->c_name}}</td>
              <td>{{$d->c_name_compared}}</td>
              <td>{{$d->value}}</td>
              <td>
                  <a href="#" class="edit" 
                    data-id="{{$d->id}}" data-c="{{$d->c_name}}" data-c_comp="{{$d->c_name_compared}}"
                    data-value="{{$d->value}}" data-toggle="modal" data-target="#criteria_value_modal">
                    <i class="material-icons" data-toggle="tooltip">&#xE254;</i>
                  </a>
              </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <input type="hidden" id="cr-criteria" value="{{$CR}}">
  <div class="alert cr-criteria" role="alert">
    Consistency ratio : <span class="cr-value">{{$CR}}</span>
  </div>

  <div class="table-wrapper">
    <div class="table-title">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <h2><b>Criteria Weight</b> Data</h2>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table id="criteria-weight-table" class="table table-striped table-hover table-text-center">
        <thead>
          <tr>
            <th>No.</th>
            <th>Criteria</th>
            <th>Weight</th>
          </tr>
        </thead>
        <tbody>
          @foreach($weight as $w)
          <tr>
              <td></td>
              <td>{{$w['criteria_name']}}</td>
              <td>{{$w['weight']}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    
  </div>
@endsection