@extends('main-template', [
  'title' => 'Home', 
  'activePage'=>'dashboard',
  'activeChild'=>'none'
  ])
@section('content')
  <div class="main text-center dashboard-custom">
    <h4>RECOMMENDATON SYSTEM FRAMEWORK USING AHP METHOD AND SENTIMENT ANALISYS</h4>
    <div class="dashboard-goal">
      <h3>Goal : </h3>
      <h1>{{$goal}}</h1>
      <a href="#" data-toggle="modal" data-target="#manage-goal" 
        data-goal="{{$goal}}">
        Edit Goal
      </a>
    </div>
    <p>By : Tri Tangguh Jiwo Priyantoro</p>
    <p>2103161011</p>
  </div>
@endsection