@extends('main-template', [
  'title' => 'Alternative value', 
  'activePage'=>'alternative', 
  'activeChild'=>'cc-alternative'
  ])
@section('content')
  <nav id="nav-alternative">
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      @foreach($data as $d)
        <a class="nav-item nav-link" id="nav-{{$d['criteria_id']}}-tab" data-toggle="tab" href="#nav-{{$d['criteria_id']}}" 
        role="tab" aria-controls="nav-{{$d['criteria_id']}}" aria-selected="true">
        {{$d['criteria_name']}}</a>
      @endforeach
      <a class="nav-item nav-link" id="nav-sentiment-tab" data-toggle="tab" href="#nav-sentiment" 
        role="tab" aria-controls="nav-sentiment" aria-selected="true">
        Sentiment Score  
      </a>
    </div>
  </nav>
  <br>
  <div class="tab-content" id="nav-tabContent">
    @foreach($data as $d)
      <div class="tab-pane fade" id="nav-{{$d['criteria_id']}}" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="table-wrapper">
          <div class="table-title">
            <div class="d-flex">
              <div class="mr-auto p-2">
                <h2><b>Comparison</b> Value</h2>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-striped table-hover table-text-center long-table">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Alernative</th>
                  <th>Comparison Alternative</th>
                  <th>Value</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                @foreach($d['data'] as $c)
                  <tr id="av-row-{{$c->id}}" data-href="linknya">
                    <td></td>
                    <td>{{$c->altern_name}}</td>
                    <td>{{$c->altern_name_compared}}</td>
                    <td>{{$c->value}}</td>
                    <td>
                        <a href="#" class="edit" 
                          data-id="{{$c->id}}" data-altern="{{$c->altern_name}}" 
                          data-altern_comp="{{$c->altern_name_compared}}" data-c_name="{{$d['criteria_name']}}"
                          data-value="{{$c->value}}" data-toggle="modal" data-target="#alternative_value_modal"
                          data-criteria="{{$d['criteria_id']}}">
                          <i class="material-icons" data-toggle="tooltip">&#xE254;</i>
                        </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

        <div class="table-wrapper">
          <div class="table-title">
            <div class="d-flex">
              <div class="mr-auto p-2">
                <h2><b>Alternative Weight</b> Data</h2>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="aw-table-{{$d['criteria_id']}}" class="table table-striped table-hover table-text-center">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Altenative</th>
                  <th>Weight</th>
                </tr>
              </thead>
              <tbody>
                @foreach($d['weight'] as $w)
                <tr>
                    <td></td>
                    <td>{{$w['alternative_name']}}</td>
                    <td>{{$w['weight']}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>  
          
        </div>
      </div>
    @endforeach
    <div class="tab-pane fade" id="nav-sentiment" role="tabpanel" aria-labelledby="nav-sentiment-tab">
      <div class="table-wrapper">
        <div class="table-title">
          <div class="d-flex">
            <div class="mr-auto p-2">
              <h2><b>Sentiment Analysis</b> Score</h2>
            </div>
            <!-- <div class="ml-auto p-2">
              <i class="material-icons">refresh</i>
            </div> -->
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-hover table-text-center">
            <thead>
              <tr>
                <th>No.</th>
                <th>Altenative</th>
                <th>Weight</th>
              </tr>
            </thead>
            <tbody>
              @foreach($sentimentWeight as $w)
              <tr>
                  <td></td>
                  <td>{{$w['alternative_name']}}</td>
                  <td>{{$w['score']}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection