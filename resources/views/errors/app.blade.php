<!DOCTYPE html>
<html>
<head>
    <title>Error.</title>
    <style>
        div {
            margin-top: 50vh;
        }

        p{
            color: #7e807c;
            text-align: center;
            font-size: 5vh;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>