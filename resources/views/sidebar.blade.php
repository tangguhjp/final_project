<div id="myDiv" class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Sycom Corp.</h4>
            <h6>Hai, {{Auth::user()->username}}!</h6>
        </div>
        <ul class="list-unstyled components">
            <!-- <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li> -->
            <li id="home">
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li id="criteria">
                <a href="{{ route('criteria.index') }}">Criteria</a>
            </li>
            <li id="criteria_value">
                <a href="{{ route('criteria-value.index') }}">Criteria Value</a>
            </li>
            <li id="alternative">
                <a href="{{ route('alternative.index') }}">Alternative</a>
            </li>
            <li id="alternative_value">
                <a href="{{ route('alternative-value.index') }}">Alternative Value</a>
            </li>
            <li id="calculation">
                <a href="{{ route('count.index') }}">Calculation</a>
            </li>
        </ul>
        <button class="btn btn-danger btn-logout" 
            onclick="
                event.preventDefault();
                document.getElementById('logout-form').submit();
            ">
            <a href="{{ route('logout') }}">
                <i class="fas fa-sign-out-alt"></i>
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </button>
    </nav>

    <!-- Page Content -->
    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <h1 class="navbar-text" >{{$title}} |</h1>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="main">
                <div class="container">
                    <div class="alert alert-dismissible fade show" role="alert" id="alert-criteria">
                        <div id="alert-message">ddd</div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @if(Session::has('alert'))
                        <div class="alert alert-danger alert-dismissible fade show alert-global" role="alert">
                            <div>{{Session::get('alert')}}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success alert-dismissible fade show alert-global" role="alert">
                            <div>{{Session::get('alert-success')}}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@include('modals')

