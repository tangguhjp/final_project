@extends('main-template', [
  'title' => 'Calculation', 
  'activePage' => 'calculate',
  'activeChild' => 'none'
  ])
@section('content')
  <div class="dashboard-goal">
    <span>Goal :</span>  
    <h3 class="goal-cal">{{$goal}}</h3>
  </div>
  <div class="table-wrapper">
    <div class="table-title">
        <div class="row">
          <div class="col-sm-6">
            <h2><b>Choose</b> Criteria</h2>
          </div>
        </div>
    </div>
    <br>
    <div>
      <form action="{{route('count.store')}}" method="post">
          {{csrf_field()}}
          @foreach($criteria as $c)
          <div class="checkbox checkbox-primary">
              <input id="{{$c->id}}" class="styled custom-control-input" type="checkbox"  name="criteria[]" value="{{$c->id}}" checked>
              <label class="" for="{{$c->id}}">
                  {{$c->criteria_name}}
              </label>
          </div>
          @endforeach
          <span class="note">*) uncheck the criteria if don't want to be included in the calculation</span><br>
          <button type="submit" class="btn btn-primary">Process</button>
        </form>
    </div>    
  </div>
@endsection