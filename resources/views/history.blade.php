@extends('main-template', [
  'title' => 'History', 
  'activePage' => 'history',
  'activeChild' => 'none'
])
@section('content')
  <div class="dashboard-goal">
    <span>Goal :</span>
    <h3 class="goal-cal">{{Session::get('goal')}}</h3>
  </div>
  <div class="table-wrapper">
    <div class="table-title">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <h2><b>Histories</b></h2>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-hover table-text-center">
        <thead>
          <tr>
            <th>Ranking</th>
            <th>Alternative</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          @if(Session::has('history'))
            @foreach(Session::get('history') as $d)
            <tr>
                <td></td>
                <td> <a href="{{route('alternative.show', $d->id)}}">{{$d->name}}</a></td>
                <td>{{$d->score}}</td>
            </tr>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
   
  </div>
@endsection