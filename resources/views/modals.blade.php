<!-- Add Criteria Modal --> 
<div id="add_criteria_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        @csrf
        <div class="modal-header">						
          <h4 class="modal-title">Add Criteria</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Criteria Name</label>
            <input type="text" class="form-control" name="criteria_name" required>
          </div>
          <div class="form-group">
            <label>Criteria Type</label><br>
            <span class="custom-checkbox">
              <input id="criteria_type" type="checkbox" id="checkbox1" name="criteria_type">
              <label for="checkbox1">Reverse criteria</label>
            </span>
          </div>
          <div class="form-group">
            <input type="hidden" class="form-control" name="identifier" value="web">
            <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
          </div>					
        </div>
        <div class="modal-footer">
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
          <button id="ajax-add-criteria" type="button" class="btn btn-success">Add</button>
          <!-- <input  class="btn btn-success" value="Add"> -->
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit Criteria Modal -->
<div id="edit_criteria_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header">						
          <h4 class="modal-title">Edit Criteria</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">					
          <div class="form-group">
            <label>Criteria Name</label>
            <input type="text" class="form-control" data-event="Dd" name="criteria_name" id="criteria_name" required>
          </div>
          <div class="form-group">
            <input type="hidden" class="form-control" name="identifier" value="web">
            <input type="hidden" class="form-control" name="criteria_id" id="criteria_id">
          </div>					
        </div>
        <div class="modal-footer">
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
          <input id="ajax-update-criteria" type="button" class="btn btn-info" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div id="delete_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form >
          <div class="modal-header">						
            <h4 class="modal-title">Delete {{$title}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">					
            <p>Are you sure you want to delete this {{$title}}?</p>
            <p class="text-warning"><small>This action cannot be undone.</small></p>
            <input type="hidden" name="delete_id" id="delete_id">
            <input type="hidden" name="identifier" id="identifier" value="web">
          </div>
          <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input  id="delete-criteria-alternative" type="button" class="btn btn-danger" data-dismiss="modal" value="Delete">
          </div>
        </form>
    </div>
  </div>
</div>

<!-- Alternative Modal Form -->
<div id="add_alternative_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="alternative-form">
        @include('alt-form')
        <div class="modal-footer">
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
          <input id="ajax-add-alternative" type="button" data-action="add" class="btn btn-success" value="Add">
      </div>
      </form>
    </div>
  </div>
</div>

<div id="edit_alternative_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="alternative-form">      
        @include('alt-form')
        <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
            <input id="ajax-edit-alternative" type="button" data-action="edit" class="btn btn-success" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Criteria Value Modal -->
@if($activeChild=="cc-criteria")
  <div id="criteria_value_modal" class="modal fade value-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <form>
          <div class="modal-header">						
              <h4 id="alternative-modal-title" class="modal-title">Edit Criteria Value</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
              <p>Pairwise comparison between "<span class="c_name"></span>" with criteria 
              "<span class="c_name_compared"></span>" :</p>					
              <div class="form-group">
                <select id="select_criteria_value"class="form-control" name="value" id="">
                  @foreach($preferences as $p)
                    <option data-info="{{$p->name}}" value="{{$p->value}}">{{$p->value}}</option>
                  @endforeach
                </select>
              </div>
              <p>
                Criterion "<span class="c_name"></span>" is <span><b class="note"></b></span> 
                than "<span class="c_name_compared"></span>"
              </p>
              <div class="form-group">
                <input type="hidden" class="form-control" name="identifier" value="web">
                <input type="hidden" class="form-control" name="id" id="id">
              </div>					
          </div>
          <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
              <input id="ajax-update-criteria-value" type="button" class="btn btn-success" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>
@endif

<!-- Alternative Value Modal -->
@if($activeChild=="cc-alternative")
  <div id="alternative_value_modal" class="modal fade value-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="alternative-form" action="{{route('alternative-value.update', 'update')}}" method="post">
          {{method_field('patch')}}
          {{csrf_field()}}
          <div class="modal-header">						
              <h4 id="alternative-value-title" class="modal-title">Edit Criteria Value</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
              <p>Pairwise comparison between "<span class="altern"></span>" with 
              "<span class="altern_comp"></span>" :</p>					
              <div class="form-group">
                <select id="select_alternative_value"class="form-control" name="value" id="">
                  @foreach($preferences as $p)
                  <option data-info="{{$p->name}}" value="{{$p->value}}">{{$p->value}}</option>
                  @endforeach
                </select>
              </div>
              <p>
                "<span class="altern"></span>" is <span><b class="note"></b></span> 
                than "<span class="altern_comp"></span>"
              </p>
              <div class="form-group">
                <input type="hidden" class="form-control" name="identifier" value="web">
                <input type="hidden" class="form-control" name="id" id="id">
                <input type="hidden" class="form-control" name="criteria_id" id="mav-criteria_id">
              </div>					
          </div>
          <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
              <input id="ajax-update-alternative-value" type="button" class="btn btn-success" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>
@endif

<div id="manage-goal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="alternative-form" action="{{route('manage-goal')}}" method="post">
          {{csrf_field()}}
          <div class="modal-header">						
              <h4 id="alternative-modal-title" class="modal-title">Edit Goal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">				
              <div class="form-group">
                <input id="goal-modal" type="text" class="form-control" name="goal">
              </div>
          </div>
          <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
              <input id="btn-submit" type="submit" class="btn btn-success" value="Save">
          </div>
        </form>
    </div>
  </div>
</div>

<div id="manage-users" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="users-form" action="{{route('user.store')}}" method="post">
          {{csrf_field()}}
          <div class="modal-header">						
              <h4 id="alternative-modal-title" class="modal-title">Edit/Tambah Users</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">				
              <div class="form-group">
                <label for="user-name">Name</label>
                <input id="user-name" type="text" class="form-control" name="name">
              </div>
              <div class="form-group">
                <label for="user-username">Username</label>
                <input id="user-username" type="text" class="form-control" name="username">
              </div>
              <div class="form-group">
                <label for="user-email">Email</label>
                <input id="user-email" type="email" class="form-control" name="email">
              </div>
              <div class="form-group">
                <label for="user-password">Password</label>
                <input id="user-password" type="password" class="form-control" name="password">
              </div>
              <div class="form-group">
                <label>Type User</label><br>
                <select id="select-role-user"class="form-control" name="role_id" >
                    <option data-info="x" value="1">Regular user</option>
                    <option data-info="x" value="2">Admin</option>
                </select>
              </div>
          </div>
          <div class="modal-footer">
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
              <input id="btn-submit" type="submit" class="btn btn-success" value="Save">
          </div>
        </form>
    </div>
  </div>
</div>
