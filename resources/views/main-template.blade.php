<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Welcome to Sycom Corp.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link rel="stylesheet" href="{{ asset('css') }}/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css') }}/bootstrap/awesome-bootstrap-checkbox.css">
    <link rel="stylesheet" href="{{ asset('main') }}/css/googleapis.css">
    <link rel="stylesheet" href="{{ asset('main') }}/css/googleapis-material-icon.css">
    <link rel="stylesheet" href="{{ asset('main') }}/css/font-awesome.min.css?v=3.2.1">
    <link rel="stylesheet" href="{{ asset('main') }}/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('main') }}/css/dataTables.bootstrap4.min.css">
    <!-- Compiled and minified CSS -->

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css') }}/custom_style.css">

    <link href="{{ asset('template-ui') }}/main.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />

</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <img class="sycom-logo" src="{{ asset('image') }}/logo.jpg" alt="">
                <!-- <div>Sycom Corp. Logo</div> -->
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>{{$title}} |</span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-right"> 
                    <span>Hai, <b>{{Auth::user()->name}}</b></span>
                    <i class="metismenu-icon pe-7s-user"></i>
                </div>
            </div>
        </div>      
            <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    
                    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li>
                                    <a id="dashboard" href="{{route('home')}}">
                                        <i class="metismenu-icon pe-7s-home"></i>
                                        Dashboard
                                    </a>
                                </li>
                                <li>
                                    <a id="criteria" href="#">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        Criteria
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul class="child-menu">
                                        <li>
                                            <a id="cd-criteria" href="{{route('criteria.index')}}">
                                                <i class="metismenu-icon"></i>
                                                Data criteria
                                            </a>
                                        </li>
                                        <li>
                                            <a id="cc-criteria" href="{{route('criteria-value.index')}}">
                                                <i class="metismenu-icon"></i>
                                                Comparison value
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a id="alternative" href="#">
                                        <i class="metismenu-icon pe-7s-car"></i>
                                        Alternative
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul class="child-menu">
                                        <li>
                                            <a id="cd-alternative" href="{{route('alternative.index')}}">
                                                <i class="metismenu-icon"></i>
                                                Data alternative
                                            </a>
                                        </li>
                                        <li>
                                            <a id="cc-alternative" href="{{route('alternative-value.index')}}">
                                                <i class="metismenu-icon"></i>
                                                Comparison value
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a id="calculate" href="{{route('count.index')}}">
                                        <i class="metismenu-icon pe-7s-calculator"></i>
                                        Calculate
                                    </a>
                                </li>
                                <li>
                                    <a id="history" href="{{route('history')}}">
                                        <i class="metismenu-icon pe-7s-timer"></i>
                                        History
                                    </a>
                                </li>
                                @if(Auth::user()->role_id == 2)
                                <li>
                                    <a id="manage-user" href="{{route('user-page')}}">
                                        <i class="metismenu-icon pe-7s-users"></i>
                                        Manage User
                                    </a>
                                </li>
                                @endif
                                <hr>
                                <li>
                                    <a href="#" class="btn-logout" 
                                        onclick="
                                            event.preventDefault();
                                            document.getElementById('logout-form').submit();
                                        ">
                                        <i class="metismenu-icon pe-7s-power"></i>
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>    
                <div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-dismissible fade show" role="alert" id="alert-criteria">
                                    <div id="alert-message">ddd</div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @if(Session::has('alert'))
                                    <div id="ds-alert" class="alert alert-danger alert-dismissible fade show alert-global" role="alert">
                                        <div>{{Session::get('alert')}}</div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if(Session::has('alert-success'))
                                    <div id="ds-alert" class="alert alert-success alert-dismissible fade show alert-global" role="alert">
                                        <div>{{Session::get('alert-success')}}</div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        </div>
    </div>
    @include('modals')

    <script defer src="{{ asset('main') }}/js/jquery.min.js"></script>
    <script defer src="{{ asset('main') }}/js/jquerysession.js"></script>

    <!-- dataTables -->
    <script defer src="{{ asset('main') }}/js/jquery-3.3.1.js"></script>
    <script defer src="{{ asset('main') }}/js/jquery.dataTables.min.js"></script>
    <script defer src="{{ asset('main') }}/js/dataTables.bootstrap4.min.js"></script>

    <script src="{{ asset('main') }}/js/jquery-3.3.1.slim.min.js"></script>
    <script src="{{ asset('main') }}/js/popper.min.js"></script>
    <script defer src="{{ asset('main') }}/js/solid.js"></script>
    <script defer src="{{ asset('main') }}/js/fontawesome.js"></script>
    <script src="{{ asset('main') }}/js/bootstrap.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script>
        var activePage = "{{$activePage}}";
        var activeChild = "{{$activeChild}}";
        var urlAlternative = "{{route('alternative.index')}}";
    </script>
    <!-- custom js -->
    <script src="{{ asset('main') }}/js/app.js"></script>
    <script src="{{ asset('main') }}/js/custom.js"></script>
    <script type="text/javascript" src="{{ asset('template-ui') }}/assets/scripts/main.js"></script></body>
</html>
