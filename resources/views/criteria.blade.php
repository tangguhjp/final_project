@extends('main-template', [
  'title' => 'Criteria', 
  'activePage'=>'criteria',
  'activeChild'=>'cd-criteria'
  ])
@section('content')

  <div class="table-wrapper">
    <div class="table-title">
        <div class="d-flex">
            <div class="mr-auto p-2">
              <h2><b>Criteria</b> Data</h2>
            </div>
            <div class="ml-auto p-2">
              <a id="add_criteria" href="#add_criteria_modal" class="btn btn-success" data-toggle="modal">
                <i class="material-icons">&#xE147;</i> 
                <span class="span-add" >Add New Criteria</span>
              </a>  
            </div>
        </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-hover table-text-center" id="table-criteria">
          <thead>
              <tr>
                  <th>No.</th>
                  <th>Criteria</th>
                  <th>Actions</th>
              </tr>
          </thead>
          <tbody>
              @foreach($data as $d)
              <tr id="row-{{$d->id}}">
                  <td></td>
                  <td>{{$d->criteria_name}}</td>
                  @if($d->criteria_type != 3)
                    <td>
                        <a href="#" class="edit" 
                          data-id="{{$d->id}}" data-name="{{$d->criteria_name}}" data-toggle="modal" data-target="#edit_criteria_modal">
                          <i class="material-icons" data-toggle="tooltip">&#xE254;</i>
                        </a>
                        <a href="#delete_modal" class="delete" data-toggle="modal" data-id="{{$d->id}}">
                          <i class="material-icons" data-toggle="tooltip">&#xE872;</i>
                        </a>
                    </td>
                  @else
                    <td> - </td>
                  @endif
              </tr>
              @endforeach
          </tbody>
      </table>
    </div>
  </div>
@endsection