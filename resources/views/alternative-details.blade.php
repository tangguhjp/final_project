@extends('main-template', [
  'title' => 'Details', 
  'activePage' => 'alternative-details',
  'activeChild' => 'none'
  ])
@section('content')
  <div class="card-details mt-3">
    <span class="label-details">Name :</span>  
    <div class="ml-4">
      <div class="card">
        <div class="card-body p-2">
          <p class="alternative-name">{{$data->alternative['name']}}</p>
        </div>
      </div>
    </div>
  </div>
  <hr class="m-0">
  <div class="card-details">
    <span class="label-details">Informations :</span>  
    <div class="ml-4">
      <div class="card">
        <div class="card-body p-2">
          <p>{{$data->alternative['info']}}</p>
        </div>
      </div>
    </div>
  </div>
  <hr class="m-0">
  <div class="card-details">
    <span class="label-details">Comments :</span>
    <div class="ml-4">
      @if(count($data->comments) < 1)
      <div id="no-comments" class="card text-center">
        No Comments.
      </div>
      @endif
      @foreach($data->comments as $c)
      <div class="card">
        <div class="card-body p-2">
          <label class="comment-label" for="cm">{{$c->user}}</label>
          <p>{{$c->comment}}</p>
          <div class="float-right">{{date('d M Y H:i:s', $c->comment_date)}}</div><br>
        </div>
      </div>
      @endforeach
    </div>

  </div>
@endsection