@extends('main-template', [
  'title' => 'Data alternative', 
  'activePage'=>'alternative', 
  'activeChild'=>'cd-alternative'
  ])
@section('content')
  <div class="table-wrapper">
    <div class="table-title">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <h2><b>Alternative</b> Data</h2>
        </div>
        <div class="ml-auto p-2">
          <a href="#add_alternative_modal" class="btn btn-success" data-action="add"
            data-toggle="modal">
              <i class="material-icons">&#xE147;</i> 
              <span class="span-add">Add New Alternative</span>
          </a>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table id="alternative-table" class="table table-striped table-hover table-text-center long-table">
        <thead>
          <tr>
            <th>No.</th>
            <th>Name</th>
            <!-- <th>Information</th> -->
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach($data as $d)
              <tr id="a-row-{{$d->id}}" class="clickable-row">
                  <td></td>
                  <td><a href="{{route('alternative.show', $d->id)}}">{{$d->name}}</a></td>
                  <!-- <td>{{$d->info}}</td> -->
                  <td>
                      <a href="#" class="edit" data-action="edit" data-id="{{$d->id}}" data-name="{{$d->name}}" 
                        data-info="{{$d->info}}" data-toggle="modal" data-target="#edit_alternative_modal">
                        <i class="material-icons" data-toggle="tooltip">&#xE254;</i>
                      </a>
                      <a href="#delete_modal" class="delete" data-toggle="modal" data-id="{{$d->id}}">
                        <i class="material-icons" data-toggle="tooltip">&#xE872;</i>
                      </a>
                  </td>
              </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection