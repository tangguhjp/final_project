@extends('main-template', [
  'title' => 'Recommendation', 
  'activePage' => 'calculate',
  'activeChild' => 'none'
])
@section('content')
  <div class="dashboard-goal">
    <span>Goal :</span>
    <h3 class="goal-cal">{{$goal}}</h3>
  </div>
  <div class="table-wrapper">
    <div class="table-title">
      <div class="d-flex">
        <div class="mr-auto p-2">
          <h2>Data <b>Result</b></h2>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-hover table-text-center">
        <thead>
          <tr>
            <th>Ranking</th>
            <th>Alternative</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $d)
          <tr>
              <td></td>
              <td> <a href="{{route('alternative.show', $d->id)}}">{{$d->name}}</a></td>
              <td>{{$d->score}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
   
  </div>
@endsection