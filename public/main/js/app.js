// CRITERIA

$(document).ready(function(){
    disableAddCriteria()

    token =  $('input[name=_token]').val();
    criteria_type = 2;
    $("#ajax-add-criteria").click(function(e){
        criteria_name = $('input[name=criteria_name]').val();
        if(criteria_name.length != 0){
            if($('input[name=criteria_type]').prop('checked')){
                criteria_type = 1
            }else{
                criteria_type = 2;            
            }
            $.ajax({
                type: 'post',
                url: '/criteria',
                data: {
                    _token : token,
                    criteria_name : criteria_name,
                    criteria_type : criteria_type,
                    identifier : 'web'
                },
                success: function(response){
                    console.log(response)
                    if(response.status == 200){
                        data = response.data;
                        $("#table-criteria").append("<tr id='row-"+data.id+"'>"+
                            "<td></td>"+
                            "<td>" + data.criteria_name + "</td>"+
                            "<td>"+
                                "<a href='#' class='edit' data-id='"+data.id+"' data-name='"+data.criteria_name+"' "+
                                "data-toggle='modal' data-target='#edit_criteria_modal'>"+
                                    "<i class='material-icons' data-toggle='tooltip' title='Edit'>&#xE254;</i></a>"+
                                "<a href='#delete_modal' class='delete' data-id='"+data.id+"' data-toggle='modal'>"+
                                    "<i class='material-icons' data-toggle='tooltip' title='Delete'>&#xE872;</i></a>"+
                            "</td>"+
                        "</tr>");
                        createAlert(response.message, 'alert-success')
                    }else if(response.status == 301){
                        alert(response.message)
                        window.location.href = "/home"
                    }else{
                        createAlert(response.message, 'alert-danger')
                    }
                    disableAddCriteria()
                    $('input[name=criteria_name]').val('')
                    $('input[name=criteria_type]').prop('checked', false);
                },
            });
            $("#add_criteria_modal .close").click();
        }
    });
    
    function disableAddCriteria(){
        if($('#table-criteria tr').length > 7){
            $('#add_criteria').addClass("disabled")
        }
    }

    function enableAddCriteria(){
        if($('#table-criteria tr').length > 7){
            $('#add_criteria').removeClass("disabled")
        }
    }

    $("#delete-criteria-alternative").click(function(e){
        id = $('input[name=delete_id]').val();
        
        baseURL = '/criteria/'
        deleletRowId = '#row-'+id
        if(activePage == 'alternative') {
            baseURL = '/alternative/'
            deleletRowId = '#a-row-'+id
        }else if(activePage == 'manage-user'){
            baseURL = '/user/'
            deleletRowId = '#u-row-'+id
        }

        $.ajax({
            type: 'delete',
            url: baseURL+id,
            data : {_token : token},
            success : function(response){
                if(response.status == 200){
                    createAlert(response.message, 'alert-success')
                    $(deleletRowId).remove()
                }else{
                    createAlert(response.message, 'alert-danger')
                }
            }
        })
        enableAddCriteria()
    })

    $("#ajax-update-criteria").click(function(e){
        criteria_name = $('#edit_criteria_modal input[name=criteria_name]').val()
        if(criteria_name.length != 0){
            $.ajax({
                type: 'put',
                url: '/criteria/'+$('#edit_criteria_modal input[name=criteria_id]').val(),
                data: {
                    _token : token,
                    criteria_name : criteria_name 
                },
                success : function(response){
                    if(response.status == 200){
                        data = response.data
                        newRow = "<tr id='row-"+data.id+"'>"+
                                "<td></td>"+
                                "<td>"+data.criteria_name+"</td>"+
                                "<td>"+
                                    "<a href='#' class='edit' data-id='"+data.id+"' data-name='"+data.criteria_name+"' "+
                                    "data-toggle='modal' data-target='#edit_criteria_modal'>"+
                                        "<i class='material-icons' data-toggle='tooltip' title='Edit'>&#xE254;</i></a>"+
                                    "<a href='#delete_modal' class='delete' data-id='"+data.id+"' data-toggle='modal'>"+
                                        "<i class='material-icons' data-toggle='tooltip' title='Delete'>&#xE872;</i></a>"+
                                "</td>"+
                            "</tr>"
                        $('#row-'+data.id).replaceWith(newRow)
                    }else{
                        createAlert(response.message, 'alert-danger')
                    }
                }
            })
            $("#edit_criteria_modal .close").click();
        }
    })

    $("#ajax-update-criteria-value").click(function(e){
        value = $("#select_criteria_value option:selected").text()

        if(value.length != 0){
            $.ajax({
                type: 'put',
                url: '/criteria-value/'+$('#criteria_value_modal input[name=id]').val(),
                data: {
                    _token : token,
                    value : $("#select_criteria_value option:selected").text(),
                    c_name : $(".c_name:first").text(),
                    c_name_compared : $(".c_name_compared:first").text()
                },
                success : function(response){
                    if(response.status == 200){
                        data = response.data
                        newRow = "<tr id='cv-row-"+data.id+"'>"+
                                    "<td></td>"+
                                    "<td>"+data.c_name+"</td>"+
                                    "<td>"+data.c_name_compared+"</td>"+
                                    "<td>"+data.value+"</td>"+
                                    "<td>"+
                                        "<a href='#' class='edit' data-id='"+data.id+"' data-c='"+data.c_name+"'"+
                                        "data-c_comp='"+data.c_name_compared+"'data-value='"+data.value+"'"+
                                        "data-toggle='modal' data-target='#criteria_value_modal'>"+
                                        "<i class='material-icons' data-toggle='tooltip' title='Edit Value'>&#xE254;</i>"+
                                        "</a>"+
                                    "</td>"+
                                "</tr>"
                        $('#cv-row-'+data.id).replaceWith(newRow)
                        fetchCriteria()
                        CRCheck(response.cr)
                        console.log(response)
                    }else{
                        createAlert(response.message, 'alert-danger')
                    }
                }
            })
            $("#criteria_value_modal .close").click();
        }
    })

    function createAlert(message, alertType){
        $('#alert-criteria').show(0).delay(3000).slideUp(1000, function(){
            $(this).hide(0)
        })

        $('#alert-criteria').addClass(alertType)
        $('#alert-criteria #alert-message').text(message)
    }

    function fetchCriteria(){
        $('#criteria-weight-table tbody tr').empty();
        $.ajax({
            type : 'get',
            url : '/getCriteriaWeight',
            success : function(response){
                $.each(response, function( index) {
                    data = response[index]
                    row = "<tr>"+
                                "<td></td>"+
                                "<td>"+data.criteria_name+"</td>"+
                                "<td>"+data.weight+"</td>"+
                            "</tr>"
                    $("#criteria-weight-table tbody").append(row);

                    console.log(response[index])
                });
                
                newValue = "<tr>"+
                                "<td></td>"+
                                "<td>{{$w['criteria_name']}}</td>"+
                                "<td>{{$w['weight']}}</td>"+
                            "</tr>"
                console.log(response[0].criteria_name);
            }
        })
    }

    $("#ajax-add-alternative").click(function(e){
        alternative_name = $('#add_alternative_modal input[name=name]').val()
        alternative_info = $('#add_alternative_modal textarea[name=info]').val()
        var ev = $(e.currentTarget).attr('data-action')
        
        if(alternative_name.length != 0 && alternative_info.length != 0){
            $.ajax({
                type : 'post',
                url : '/alternative',
                data : {
                    _token : token,
                    alternative_name : alternative_name,
                    alternative_info : alternative_info,
                    user_id : $('#add_alternative_modal input[name=user_id]').val(),
                    identifier : 'web'
                },
                success : function(response){
                    console.log(response)
                    if(response.status == 200){
                        data = response.data ;
                        href = urlAlternative+"/"+data.id;
                        newRow = "<tr id='a-row-"+data.id+"' class='clickable-row'>"+
                                "<td></td>"+
                                "<td> <a href='"+href+"'>"+data.name+"</a></td>"+
                                // "<td>"+data.info+"</td>"+
                                "<td>"+
                                    "<a href='#' class='edit' data-action='edit' data-id='"+data.id+"' data-name='"+data.name+"'"+ 
                                    "data-info='"+data.info+"' data-toggle='modal' data-target='#edit_alternative_modal'>"+
                                    "<i class='material-icons' data-toggle='tooltip' title='Edit'>&#xE254;</i>"+
                                    "</a>"+
                                    "<a href='#delete_modal' class='delete' data-toggle='modal' data-id='"+data.id+"'>"+
                                    "<i class='material-icons' data-toggle='tooltip' title='Delete'>&#xE872;</i>"+
                                    "</a>"+
                                "</td>"+
                            "</tr>"
                        $("#alternative-table").append(newRow); 
                        $('#add_alternative_modal input[name=name]').val('');
                        $('#add_alternative_modal textarea[name=info]').val('');
                        createAlert(response.message, 'alert-success');
                    }else{
                        createAlert(response.message, 'alert-danger');
                    }
                }
            })
            $("#add_alternative_modal .close").click();
        }
    })

    $("#ajax-edit-alternative").click(function(e){
        // e.preventDefault()
        alternative_name = $('#edit_alternative_modal input[name=name]').val()
        alternative_info = $('#edit_alternative_modal textarea[name=info]').val()
        alternative_id = $('#edit_alternative_modal input[name=alt_id]').val()
        if(alternative_name.length != 0 && alternative_info.length != 0){
            $.ajax({
                type : 'put',
                url : '/alternative/'+alternative_id,
                data : {
                    _tpken : token,
                    alternative_name : alternative_name,
                    alternative_info : alternative_info
                },
                success : function(response){
                    if(response.status == 200){
                        data = response.data;
                        href = urlAlternative+"/"+data.id;
                        newRow = "<tr id='a-row-"+data.id+"'>"+
                            "<td></td>"+
                            "<td> <a href='"+href+"'>"+data.name+"</a></td>"+
                            "<td>"+
                                "<a href='#' class='edit' data-action='edit' data-id='"+data.id+"' data-name='"+data.name+"'"+ 
                                "data-info='"+data.info+"' data-toggle='modal' data-target='#edit_alternative_modal'>"+
                                "<i class='material-icons' data-toggle='tooltip' title='Edit'>&#xE254;</i>"+
                                "</a>"+
                                "<a href='#delete_modal' class='delete' data-toggle='modal' data-id='"+data.id+"'>"+
                                "<i class='material-icons' data-toggle='tooltip' title='Delete'>&#xE872;</i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>"
                        $('#a-row-'+data.id).replaceWith(newRow)
                    }else{

                    }
                }
            })
            $("#edit_alternative_modal .close").click();

        }
    })

    $('#ajax-update-alternative-value').click(function(){
        value = $("#select_alternative_value option:selected").text()
        alternative_id = $("#alternative_value_modal input[name=id]").val()
        criteria_id = $("#alternative_value_modal input[name=criteria_id]").val()
        criteria_name = $("#alternative-value-title").text()

        if(value.length != 0){
            $.ajax({
                type : 'put',
                url : '/alternative-value/'+alternative_id,
                data : {
                    _token : token,
                    altern : $(".altern:first").text(),
                    altern_comp : $(".altern_comp:first").text(),
                    value : value
                },
                success : function(response){
                    if(response.status == 200){
                        data = response.data
                        console.log(data.id)
                        newRow = "<tr id='av-row-"+data.id+"' class='clickable-row' data-href='linknya'>"+
                                    "<td></td>"+
                                    "<td>"+data.altern+"</td>"+
                                    "<td>"+data.altern_comp+"</td>"+
                                    "<td>"+data.value+"</td>"+
                                    "<td>"+
                                        "<a href='#' class='edit' data-id='"+data.id+"' data-altern='"+data.altern+"'"+ 
                                        "data-altern_comp='"+data.altern_comp+"' data-c_name='"+criteria_name+"'"+
                                        "data-value='"+data.value+"' data-toggle='modal' data-target='#alternative_value_modal'"+
                                        "data-criteria='"+criteria_id+"'>"+
                                        "<i class='material-icons' data-toggle='tooltip' title='Edit Value'>&#xE254;</i>"+
                                        "</a>"+
                                    "</td>"+
                                "</tr>"
                        $("#av-row-"+alternative_id).replaceWith(newRow)
                        fetchAlternative(criteria_id)
                        $("#alternative_value_modal .close").click()
                    }else{

                    }
                }
            })
        }
    })

    function fetchAlternative(criteria_id){
        $("#aw-table-"+criteria_id+" tbody tr").empty();

        $.ajax({
            type : 'get',
            url : '/getAlternativeWeight/'+criteria_id,
            data : { _token : token },
            success : function(response){
                $.each(response, function( index) {
                    data = response[index]
                    console.log(data)
                    row = "<tr>"+
                                "<td></td>"+
                                "<td>"+data.alternative_name+"</td>"+
                                "<td>"+data.weight+"</td>"+
                            "<tr>"
                    $("#aw-table-"+criteria_id+" tbody").append(row);
                });
            }
        })
    }

    $('.edit-user').click(function(e){
        var id = $(this).data('id');
        $.get( "/user/"+id, function( data ) {
            $('#users-form').attr('action', '/user/'+id);
            $('#users-form').append("<input type='hidden' name='_method' value='PUT'>");

            $("#users-form input[name='name']").val(data['name']);
            $("#users-form input[name='username']").val(data['username']);
            $("#users-form input[name='email']").val(data['email']);

            $("#select-role-user option[value='"+data['role_id']+"']").prop('selected', true);
            console.log(data)
        });
    })

    $('#add-user').click(function(){
        $("#users-form .modal-body input").val('');
        $('#users-form input[name="_method"]').remove();
        $('#users-form').attr('action', '/user');

    });


    //CR custom
    // CRCheck();
    cr_criteria = $("#cr-criteria").val();
    if(cr_criteria){
        CRCheck(cr_criteria)
    }

})


$('input').keypress(function(e){
    if(e.keyCode == 13){
        e.preventDefault();
    }
})

// $("#d-alert").fade(2000, 500).slideUp(500, function() {
//     $("#d-alert").slideUp(500);
//   });
$('#ds-alert').ready(function(){
    $('#ds-alert').show(0).delay(3000).slideUp(1000, function(){
        $(this).hide(0)
    })
})

// window.setTimeout(function(){
//     $('.alert-global').fadeTo(5000, 0).slideUp(2000, function(){
//         $(this).hide()
//     })
// })

function CRCheck(cr){
    // cr_criteria = $("#cr-criteria").val();
    $(".cr-criteria .cr-value").text(cr);
    $("#cr-criteria").val(cr);
    $("div.cr-criteria").removeClass("alert-danger alert-success");

    if(cr <= 0.1){
        $("div.cr-criteria").addClass("alert-success")
    }else{
        $("div.cr-criteria").addClass("alert-danger")
    }    
}