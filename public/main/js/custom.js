$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });
});

// DataTable
$(document).ready(function() {
    var t = $('.long-table').DataTable({
        "lengthChange": false,
        "ordering":false,
        
    });
    if ($(".long-table").length) {
        $(".dataTables_empty").parents('tbody').empty();;
    }
} );

$(document).ready(function()
{
    $("table.table").removeClass("long-table");
});

$('.list-unstyled a').click(function(){
    $('.list-unstyled .active').removeClass('active');
    $(this).parent().addClass('active');
});

/**Modal action */
$('#edit_criteria_modal').on('show.bs.modal', function (event) {
    var e = $(event.relatedTarget)
    var id =e.attr('data-id')
    var name = e.attr('data-name')
    var modal = $(this)
    modal.find('.modal-body #criteria_name').val(name);
    modal.find('.modal-body #criteria_id').val(id);
})

$('#delete_modal').on('show.bs.modal', function (event) {
    var e = $(event.relatedTarget)
    var id =e.attr('data-id')
    var modal = $(this)
    modal.find('.modal-body #delete_id').val(id);
})
$('#edit_alternative_modal').on('show.bs.modal', function (event) {
    var e = $(event.relatedTarget)
    var id =e.attr('data-id')
    var name = e.attr('data-name')
    var info = e.attr('data-info')

    var modal = $(this)
    modal.find('.modal-body #alt_name').val(name);
    modal.find('.modal-body #alt_info').val(info);
    modal.find('.modal-body #alt_id').val(id);
    modal.find('.modal-body #alternative-action').val(e.attr('data-action'));
    modal.find('.modal-title').text('Edit Alternative');
})

$('#criteria_value_modal').on('show.bs.modal', function(event) {
    var e = $(event.relatedTarget)

    $("#select_criteria_value").val(e.attr("data-value"))  
    $('#select_criteria_value').on('change',function() {
        var selectedOption = $(this).find(':selected').data('info')
        $('.note').text(selectedOption);  
    }).change();
    var modal = $(this)

    modal.find('.modal-body #id').val(e.attr("data-id"))
    modal.find('.modal-body .c_name').text(e.attr("data-c"))
    modal.find('.modal-body .c_name_compared').text(e.attr("data-c_comp"))
})

$('#alternative_value_modal').on('show.bs.modal', function(event) {
    var e = $(event.relatedTarget)

    $("#select_alternative_value").val(e.attr("data-value"))  
    $('#select_alternative_value').on('change',function() {
        var selectedOption = $(this).find(':selected').data('info')
        $('.note').text(selectedOption);  
    }).change();
    var modal = $(this)

    modal.find('.modal-body #id').val(e.attr("data-id"))
    modal.find('.modal-body .altern').text(e.attr("data-altern"))
    modal.find('.modal-body .altern_comp').text(e.attr("data-altern_comp"))
    modal.find('.modal-body #mav-criteria_id').val(e.attr("data-criteria"))
    
    modal.find('#alternative-value-title').text(e.attr("data-c_name"))
})

$('#manage-goal').on('show.bs.modal', function(e){
    var goal = $(e.relatedTarget).data('goal');
    $('#goal-modal').val(goal);
    // console.log(goal);
})
/**---------------------------------- */
$(".vertical-nav-menu #"+activePage ).addClass("mm-active");
$(".child-menu #"+activeChild).addClass("mm-active");

/**Go To Top */
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    // if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    //     document.getElementById("btn-top").style.display = "block";
    // } else {
    //     document.getElementById("btn-top").style.display = "none";
    // }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
/**------------------------------------ */

$('#nav-alternative div a').eq(0).addClass('active');
$('#nav-tabContent div').eq(0).addClass('show active');

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 300);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "flex";
}

/**------------------------------------ */
