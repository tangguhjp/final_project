<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria_value', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_criteria');
            $table->unsignedInteger('id_criteria_compared');
            $table->double('value');

            $table->foreign('id_criteria')->references('id')->on('criteria')->onDelete('cascade');
            $table->foreign('id_criteria_compared')->references('id')->on('criteria')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria_value');
    }
}
