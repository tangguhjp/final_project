<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlternativeValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternative_value', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_criteria');
            $table->unsignedInteger('id_alternative');
            $table->unsignedInteger('id_alternative_compared');
            $table->double('value');

            $table->foreign('id_criteria')->references('id')->on('criteria')->onDelete('cascade');
            $table->foreign('id_alternative')->references('id')->on('alternative')->onDelete('cascade');
            $table->foreign('id_alternative_compared')->references('id')->on('alternative')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternative_value');
    }
}
