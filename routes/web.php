<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/history', function () {
    return view('history');
})->name('history')->middleware('auth');


Route::resources([ 
    'criteria' => 'CriteriaController',
    'alternative' => 'AlternativeController',
    'criteria-value' => 'CriteriaValueController', 
    'alternative-value' => 'AlternativeValueController', 
    'user' => 'UserController',
    'test' => 'TestController',
    'preferences' => 'PreferencesController',
    'count' => 'CountController'
    ]);

Route::post('/criteria-test/{test}', 'CriteriaController@critest')->name('criteria-test');
Route::get('/user-register', 'UserController@register')->name('user-register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/goal', 'HomeController@manageGoal')->name('manage-goal');

Route::get('/getCriteriaWeight', 'CriteriaValueController@getCriteriaWeight')->name('getCriteriaWeight');
Route::get('/getAlternativeWeight/{criteria_id}', 'AlternativeValueController@getAlternativeWeight')->name('getAlternativeWeight');

Route::get('/user-page', 'UserController@getUsers')->name('user-page')->middleware('auth');
