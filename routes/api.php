<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    'user' => 'UserController',
    'criteria' => 'CriteriaController',
    'criteria-value' => 'CriteriaValueController',
    'process' => 'ProcessController',
    'preferences' => 'PreferencesController',
    'comment' => 'CommentsController'
    ]);

Route::get('/criteria-user/{user_id}', 'CriteriaController@userCriteria')->name('criteria-user');
Route::get('/alternative-user/{user_id}', 'AlternativeController@userAlternative')->name('alternative-user');
// Route::resource('test', 'TestController')->middleware('auth:api');

Route::group(['middleware' => 'auth:api'], function() {
      Route::resource('test', 'TestController');
      Route::resource('comment', 'CommentsController');
  });

Route::post('/login', 'API\ApiController@apiLogin');
Route::post('/addCriteria', 'TestController@addCriteria')->name('addCriteria');

// API Testing
Route::post('/tweets', 'TestController@getPost');
Route::post('/sentiment', 'TestController@getSentiment');
Route::post('/correct-word', 'TestController@correctWord');
